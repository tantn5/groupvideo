package com.groupcall;

import android.app.Application;

import com.groupcall.common.Common;
import com.groupcall.common.UserManager;
import com.groupcall.model.User;
import com.groupcall.network.NetworkManager;

/**
 * Created by tantn
 */

public class CusApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Common.context = this;
        Common.me = new User();
        Common.me.setName("name_" + System.currentTimeMillis());
        UserManager.getInstance().add(Common.me);

        //connect to server
        NetworkManager.getInstance().connect();
    }
}
