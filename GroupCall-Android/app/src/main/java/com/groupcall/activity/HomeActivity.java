package com.groupcall.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.groupcall.R;
import com.groupcall.common.Common;
import com.groupcall.common.Notify;
import com.groupcall.common.UserManager;
import com.groupcall.common.Utils;
import com.groupcall.model.User;
import com.groupcall.network.SendPacketUtils;
import com.webrtc.WebRtcListener;

import org.json.JSONObject;
import org.webrtc.EglBase;
import org.webrtc.MediaStream;
import org.webrtc.SessionDescription;
import org.webrtc.SurfaceViewRenderer;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class HomeActivity extends AppCompatActivity {
    private BroadcastReceiver joinRoomReceiver;
    private BroadcastReceiver sdpAnswerReceiver;
    private BroadcastReceiver iceCandidateReceiver;
    private BroadcastReceiver newMemberReceiver;
    private BroadcastReceiver leaveRoomReceiver;

    private static final String TAG = HomeActivity.class.getSimpleName();
    private LinearLayout vContainVideo;
    private static final String roomname = "RoomTest";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        vContainVideo = (LinearLayout) findViewById(R.id.v_contain_video);

        //delay 3s de connect to server
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                joinRoom();
            }
        }, 3000);

        registerBroadcastReceiver();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterBroadcastReceiver();
    }

    private void joinRoom() {
        //start peerConnection
        SendPacketUtils.joinRoom(roomname, Common.me.getName());
    }

    private SurfaceViewRenderer addViewVideo() {
        View rowRender = getLayoutInflater().inflate(R.layout.row_video, null);
        vContainVideo.addView(rowRender);
        SurfaceViewRenderer render = (SurfaceViewRenderer) rowRender.findViewById(R.id.sv_video);
        EglBase eglBase = EglBase.create();
        render.init(eglBase.getEglBaseContext(), null);
        render.setTag(rowRender);
        return render;
    }

    private void registerBroadcastReceiver() {
        joinRoomReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    String roomName = intent.getStringExtra("roomName");
                    String name = intent.getStringExtra("name");
                    if (name.equals(Common.me.getName()) && name.equals("phu")) {//minh join room thanh cong
                        Common.me.setRoomName(roomName);
                        Common.me.startSendVideo(HomeActivity.this, addViewVideo());
                    }
                    List<User> users = intent.getParcelableArrayListExtra("users");
                    for (User u : users) {//fai get user tu map de lay cac object nhu peerclient
                        User user = UserManager.getInstance().get(u.getName());
                        if (user != null && user.getName().equals("phu")) {
                            user.startRecvVideo(HomeActivity.this, addViewVideo());
                        }
                    }
                } catch (Exception e) {
                    Utils.log(TAG, e.getMessage());
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(joinRoomReceiver, new IntentFilter(Notify.JOIN_ROOM.getValue()));

        sdpAnswerReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    String name = intent.getStringExtra("name");
                    String sdpAnswer = intent.getStringExtra("sdpAnswer");
                    User user = UserManager.getInstance().get(name);
                    user.getPeerConnectionClient().setRemoteDescription(new JSONObject(sdpAnswer));
                } catch (Exception e) {
                    Utils.log(TAG, e.getMessage());
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(sdpAnswerReceiver, new IntentFilter(Notify.SDP_ANSWER.getValue()));

        iceCandidateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    String name = intent.getStringExtra("name");
                    String sdpMid = intent.getStringExtra("sdpMid");
                    int sdpMLineIndex = intent.getIntExtra("sdpMLineIndex", 0);
                    String candidate = intent.getStringExtra("candidate");
                    User user = UserManager.getInstance().get(name);
                    user.getPeerConnectionClient().addIceCandidate(sdpMid, sdpMLineIndex, candidate);
                } catch (Exception e) {
                    Utils.log(TAG, e.getMessage());
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(iceCandidateReceiver, new IntentFilter(Notify.ICE_CANDIDATE.getValue()));

        newMemberReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    User u = intent.getParcelableExtra("user");
                    User user = UserManager.getInstance().get(u.getName());
                    if (user != null && user.getName().equals("phu")) {
                        user.startRecvVideo(HomeActivity.this, addViewVideo());
                    }
                } catch (Exception e) {
                    Utils.log(TAG, e.getMessage());
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(newMemberReceiver, new IntentFilter(Notify.NEW_MEMBER.getValue()));
        leaveRoomReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    String name = intent.getStringExtra("name");
                    String roomName = intent.getStringExtra("roomName");
                    //remove stream cua user nay
                    if (!name.equals(Common.me.getName()) && roomName.equals(Common.me.getRoomName())) {
                        User user = UserManager.getInstance().get(name);
                        if (user != null) {
                            user.stopVideo();
                            vContainVideo.removeView((View) user.getRenderer().getTag());
                        }
                    }
                } catch (Exception e) {
                    Utils.log(TAG, e.getMessage());
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(leaveRoomReceiver, new IntentFilter(Notify.LEAVE_ROOM.getValue()));
    }

    private void unregisterBroadcastReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(joinRoomReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(sdpAnswerReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(iceCandidateReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(newMemberReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(leaveRoomReceiver);
    }
}
