package com.groupcall.common;

import com.groupcall.network.app.AppServiceType;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tantn
 */

public enum Notify {
    JOIN_ROOM(".join_room"),
    SDP_ANSWER(".sdp_answer"),
    ICE_CANDIDATE(".ice_candidate"),
    NEW_MEMBER(".new_member"),
    LEAVE_ROOM(".leave_room");

    static final Map<String, Notify> types = new HashMap<>();

    static {
        final Notify[] all = Notify.values();
        for (Notify clientServiceType : all) {
            types.put(clientServiceType.getValue(), clientServiceType);
        }
    }

    private String value;

    public static Notify getServiceType(String value) {
        return types.get(value);
    }

    private Notify(String value) {
        this.value = value;
    }

    public String getValue() {
        return "com.groupcall" + value;
    }
}
