package com.groupcall.common;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.groupcall.model.User;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tantn
 */

public class NotifyService {
    public static void notifyJoinRoom(String roomName, String name, ArrayList<User> users){
        Intent intent = new Intent(Notify.JOIN_ROOM.getValue());
        intent.putExtra("roomName", roomName);
        intent.putExtra("name", name);
        intent.putExtra("users", users);
        LocalBroadcastManager.getInstance(Common.context).sendBroadcast(intent);
    }

    public static void notifyNewMember(User user){
        Intent intent = new Intent(Notify.NEW_MEMBER.getValue());
        intent.putExtra("user", user);
        LocalBroadcastManager.getInstance(Common.context).sendBroadcast(intent);
    }

    public static void notifySdpAnswer(String name, String sdpAnswer){
        Intent intent = new Intent(Notify.SDP_ANSWER.getValue());
        intent.putExtra("name", name);
        intent.putExtra("sdpAnswer", sdpAnswer);
        LocalBroadcastManager.getInstance(Common.context).sendBroadcast(intent);
    }


    public static void notifyIceCandidate(String name, String sdpMid, int sdpMLineIndex, String candidate){
        Intent intent = new Intent(Notify.ICE_CANDIDATE.getValue());
        intent.putExtra("name", name);
        intent.putExtra("sdpMid", sdpMid);
        intent.putExtra("sdpMLineIndex", sdpMLineIndex);
        intent.putExtra("candidate", candidate);
        LocalBroadcastManager.getInstance(Common.context).sendBroadcast(intent);
    }

    public static void notifyLeaveRoom(String roomName, String name){
        Intent intent = new Intent(Notify.LEAVE_ROOM.getValue());
        intent.putExtra("roomName", roomName);
        intent.putExtra("name", name);
        LocalBroadcastManager.getInstance(Common.context).sendBroadcast(intent);
    }
}
