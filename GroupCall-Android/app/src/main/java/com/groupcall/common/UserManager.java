package com.groupcall.common;

import com.groupcall.model.User;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tantn
 */

public class UserManager {
    private static UserManager instance;
    private Map<String, User> users = new HashMap<>();

    public static UserManager getInstance() {
        if (instance == null) {
            instance = new UserManager();
        }
        return instance;
    }

    public void add(User user) {
        users.put(user.getName(), user);
    }

    public User get(String name) {
        return users.get(name);
    }

    public void remove(String name) {
        users.remove(name);
    }
}
