package com.groupcall.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.groupcall.common.Common;
import com.groupcall.common.Utils;
import com.groupcall.network.SendPacketUtils;
import com.webrtc.PeerConnectionClient;
import com.webrtc.WebRtcListener;

import org.json.JSONObject;
import org.webrtc.MediaStream;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoRenderer;
import org.webrtc.VideoTrack;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by tantn
 */

public class User implements Parcelable {
    private String name;
    private String roomName;
    private PeerConnectionClient peerConnectionClient = new PeerConnectionClient();
    private SurfaceViewRenderer renderer;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public PeerConnectionClient getPeerConnectionClient() {
        return peerConnectionClient;
    }

    public void setPeerConnectionClient(PeerConnectionClient peerConnectionClient) {
        this.peerConnectionClient = peerConnectionClient;
    }

    public User() {
    }

    public User(Parcel in) {
        name = in.readString();
        roomName = in.readString();
    }

    public void writeToParcel(Parcel dest, int flag) {
        dest.writeString(name);
        dest.writeString(roomName);
    }

    public int describeContents() {
        return 0;
    }


    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public SurfaceViewRenderer getRenderer(){
        return renderer;
    }

    public void startSendVideo(Context context, SurfaceViewRenderer renderer) {
        //send only
        startVideo(context, renderer, true);
    }

    public void startRecvVideo(Context context, SurfaceViewRenderer renderer) {
        //recv only
        startVideo(context, renderer, false);
    }

    private void startVideo(Context context, SurfaceViewRenderer rd, boolean isSendOnly) {
        this.renderer = rd;
        getPeerConnectionClient().createPeerConnection(context, renderer, new WebRtcListener() {
            @Override
            public void onIceCandidateReceiver(JSONObject iceCandidate) {
                SendPacketUtils.iceCandidate(getName(), iceCandidate);
            }

            @Override
            public void onSdpReceiver(JSONObject sdp) {
                SendPacketUtils.offer(getName(), sdp);
            }

            @Override
            public void onAddStream(final MediaStream stream) {
                Utils.log(TAG, "onAddStream: videoTracks size =" + stream.videoTracks.size()
                        + "\n audioTracks size =" + stream.audioTracks.size());
                if (stream.videoTracks.size() == 0) {
                    return;
                }
                renderer.post(new Runnable() {
                    @Override
                    public void run() {
                        VideoRenderer videoRenderer = new VideoRenderer(renderer);
                        VideoTrack videoTrack = stream.videoTracks.get(0);
                        videoTrack.addRenderer(videoRenderer);
                        videoTrack.setEnabled(true);
                    }
                });
            }

            @Override
            public void onRemoveStream(MediaStream stream) {
            }
        }, isSendOnly);
    }

    public void stopVideo() {
        this.getPeerConnectionClient().endCall();
        this.setRoomName(null);
    }
}
