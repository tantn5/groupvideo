package com.groupcall.network;

import com.groupcall.common.Constant;
import com.groupcall.network.app.AppPacket;

import java.net.InetSocketAddress;

/**
 * Created by tantn
 */

public class NetworkManager {
    private static NetworkManager instance;
    private TcpClient tcpClient;

    public static NetworkManager getInstance() {
        if (instance == null) {
            instance = new NetworkManager();
        }
        return instance;
    }

    public void connect() {
        if (tcpClient == null) {
            tcpClient = new TcpClient(new InetSocketAddress(Constant.ip, Constant.port));
        }
        tcpClient.start();
    }

    public void sendPacket(AppPacket appPacket) {
        if (tcpClient != null) {
            tcpClient.send(appPacket);
        }
    }
}
