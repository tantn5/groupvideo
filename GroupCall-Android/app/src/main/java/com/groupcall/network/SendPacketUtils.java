package com.groupcall.network;

import com.groupcall.network.app.AppPacket;
import com.groupcall.network.app.AppServiceType;

import org.json.JSONObject;

/**
 * Created by tantn
 */

public class SendPacketUtils {
    public static void joinRoom(String roomName, String name) {
        AppPacket appPacket = new AppPacket(AppServiceType.JOIN_ROOM.getValue());
        appPacket.put("roomName", roomName);
        appPacket.put("name", name);
        NetworkManager.getInstance().sendPacket(appPacket);
    }

    public static void leaveRoom(String name) {
        AppPacket appPacket = new AppPacket(AppServiceType.LEAVE_ROOM.getValue());
        appPacket.put("roomName", name);
        NetworkManager.getInstance().sendPacket(appPacket);
    }

    public static void iceCandidate(String toName, JSONObject candidate) {
        AppPacket appPacket = new AppPacket(AppServiceType.ICE_CANDIDATE.getValue());
        appPacket.put("name", toName);
        appPacket.put("candidate", candidate);
        NetworkManager.getInstance().sendPacket(appPacket);
    }

    public static void offer(String toName,JSONObject sdpOffer) {
        AppPacket appPacket = new AppPacket(AppServiceType.SDP_OFFER.getValue());
        appPacket.put("sdpOffer", sdpOffer);
        appPacket.put("name", toName);
        NetworkManager.getInstance().sendPacket(appPacket);
    }
}
