package com.groupcall.network;

import com.groupcall.common.Utils;
import com.groupcall.network.app.AppPacket;
import com.groupcall.network.app.AppWorker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;

/**
 * Created by tantn
 */

public class TcpClient extends Thread {
    private static final String TAG = TcpClient.class.getSimpleName();

    private final SocketAddress socketAddress;
    private final Socket socket;
    private InputStream inputStream;
    private OutputStream outputStream;

    private int connectTimeout = 20000;//20s
    private final ByteBuffer readBuffer = ByteBuffer.allocateDirect(0x100000);// 1MB

    public TcpClient(InetSocketAddress inetSocketAddress) {
        super("io-thread");
        this.socketAddress = inetSocketAddress;
        this.socket = new Socket();
    }

    //ket noi den server
    public boolean connect(int connectTimeout) {
        this.connectTimeout = connectTimeout;
        try {
            socket.connect(socketAddress, connectTimeout);
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            return true;
        } catch (IOException ex) {
            Utils.log(TcpClient.class.getSimpleName(), ex.getMessage());
        }
        return false;
    }

    @Override
    public void run() {
        //neu chua ket noi thi thuc hien ket noi
        if (!socket.isConnected()) {
            connect(connectTimeout);
        }

        //kiem tra ket noi thanh cong chua
        if (socket.isConnected()) {
            Utils.log(TAG, "Connected");
            while (socket.isConnected()) {
                try {
                    //doc du lieu tu client
                    byte[] buffer = new byte[4096];
                    int read = inputStream.read(buffer);
                    if (read > 0) {
                        readBuffer.put(buffer, 0, read);
                        readBuffer.flip();
                        recv(readBuffer);
                        readBuffer.compact();
                    } else if (read < 0) {
                        break;
                    }
                } catch (IOException ex) {
                    Utils.log(TcpClient.class.getSimpleName(), ex.getMessage());
                    break;
                }
            }
        }

        Utils.log(TAG, "Diconnected");
        disconnect();
    }

    //ngat ket noi va giai phong tai nguyen
    public void disconnect() {
        if (socket.isConnected()) {
            try {
                socket.close();
            } catch (IOException e) {
                Utils.log(TcpClient.class.getSimpleName(), e.getMessage());
            }
        }
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                Utils.log(TcpClient.class.getSimpleName(), e.getMessage());
            }
        }
        if (outputStream != null) {
            try {
                outputStream.close();
            } catch (IOException e) {
                Utils.log(TcpClient.class.getSimpleName(), e.getMessage());
            }
        }
    }

    public boolean send(AppPacket packet) {
        Utils.log(TAG, "Send: " + packet);

        ByteBuffer buffer = ByteBuffer.allocate(packet.getLenght() + AppPacket.header_lenght);
        buffer.put(AppPacket.magic.getBytes());
        buffer.putShort(packet.getLenght());
        buffer.putShort(packet.getService());
        buffer.put(packet.getData());
        buffer.flip();

        return send(buffer.array(), 0, buffer.remaining());
    }

    public boolean send(byte[] bytes, int offset, int length) {
        if (socket.isConnected()) {
            try {
                outputStream.write(bytes, offset, length);
                return true;
            } catch (IOException e) {
                Utils.log(TcpClient.class.getSimpleName(), e.getMessage());
            }
        }
        return false;
    }

    public void recv(ByteBuffer buf) {
        while (buf.remaining() > 0) {
            // Doc header ------------------------- ->
            if (buf.remaining() < 6) {
                //Neu chua du 6 byte header
                return;
            }
            buf.mark();

            byte[] bytes = new byte[2];
            buf.get(bytes);
            String magic = new String(bytes);

            short length = buf.getShort();
            short service = buf.getShort();
            // Doc header ------------------------- <-

            AppPacket packet = new AppPacket(service);

            // Doc body
            if (length > 0) {
                if (buf.remaining() < length) {
                    buf.reset();
                    return;
                }
                byte[] data = new byte[length];
                if (length > 0) {
                    buf.get(data);
                }
                try {
                    packet.setJsonData(new JSONObject(new String(data, "UTF-8")));
                } catch (UnsupportedEncodingException ex) {
                    try {
                        packet.setJsonData(new JSONObject(new String(data)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.log(TAG, ex.getMessage());
                } catch (JSONException ex) {
                    Utils.log(TAG, ex.getMessage());
                }
            }
            Utils.log(TAG, "Receiver: " + packet);
            AppWorker.handler(packet);
        }
    }
}
