package com.groupcall.network.app;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by tantn
 */

public class AppPacket {
    public static final int header_lenght = 6;//magic+service+lenght
    public static final String magic = "GC";

    //data
    private short service;
    private JSONObject jsonData = new JSONObject();

    public AppPacket(short service) {
        this.service = service;
    }

    public short getService() {
        return service;
    }

    public void setService(short service) {
        this.service = service;
    }

    public JSONObject getJsonData() {
        return jsonData;
    }

    public void setJsonData(JSONObject jsonData) {
        this.jsonData = jsonData;
    }

    public void put(String key, Object value) {
        try {
            jsonData.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private byte[] data;

    public byte[] getData() {
        if (data == null) {
            encodeJson();
        }
        return data;
    }

    private void encodeJson() {
        try {
            data = jsonData.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
    }

    public short getLenght() {
        if (data == null) {
            encodeJson();
        }
        return (short) data.length;
    }

    @Override
    public String toString() {
        return "Service: " + AppServiceType.getServiceType(service) + " | " + "DATA=" + jsonData.toString();
    }
}
