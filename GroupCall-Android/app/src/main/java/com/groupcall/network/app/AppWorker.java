package com.groupcall.network.app;

import com.groupcall.common.Utils;
import com.groupcall.network.worker.IceCandidateProcessor;
import com.groupcall.network.worker.JoinRoomProcessor;
import com.groupcall.network.worker.LeaveRoomProcessor;
import com.groupcall.network.worker.NewMemberProcessor;
import com.groupcall.network.worker.ProcessorBase;
import com.groupcall.network.worker.SdpAnswerProcessor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tantn
 */
public class AppWorker {
    private static final String TAG = AppWorker.class.getSimpleName();
    private static final Map<Short, ProcessorBase> mapProcessor = new HashMap<Short, ProcessorBase>();

    public AppWorker() {
    }

    static {
        mapProcessor.put(AppServiceType.JOIN_ROOM.getValue(), new JoinRoomProcessor());
        mapProcessor.put(AppServiceType.SDP_ANSWER.getValue(), new SdpAnswerProcessor());
        mapProcessor.put(AppServiceType.ICE_CANDIDATE.getValue(), new IceCandidateProcessor());
        mapProcessor.put(AppServiceType.NEW_MEMBER.getValue(), new NewMemberProcessor());
        mapProcessor.put(AppServiceType.LEAVE_ROOM.getValue(), new LeaveRoomProcessor());
    }

    public static void handler(AppPacket appPacket) {
        ProcessorBase processorBase = mapProcessor.get(appPacket.getService());
        if (processorBase != null) {
            processorBase.process(appPacket);
        } else {
            Utils.log(TAG, "Could not found processor for: " + appPacket.toString());
        }
    }
}
