package com.groupcall.network.worker;

import com.groupcall.common.NotifyService;
import com.groupcall.common.UserManager;
import com.groupcall.common.Utils;
import com.groupcall.model.User;
import com.groupcall.network.app.AppPacket;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by tantn
 */
public class IceCandidateProcessor extends ProcessorBase {
    private static final String TAG = IceCandidateProcessor.class.getSimpleName();

    public void process(AppPacket appPacket) {
        try {
            JSONObject jsonObject = appPacket.getJsonData();
            String name = jsonObject.getString("name");
            JSONObject jsonCandidate = jsonObject.getJSONObject("candidate");
            String sdpMid = jsonCandidate.getString("sdpMid");
            int sdpMLineIndex = jsonCandidate.getInt("sdpMLineIndex");
            String candidate = jsonCandidate.getString("candidate");
            NotifyService.notifyIceCandidate(name, sdpMid, sdpMLineIndex, candidate);
        } catch (Exception e) {
            Utils.log(TAG, e.getMessage());
        }
    }
}
