package com.groupcall.network.worker;

import com.groupcall.common.NotifyService;
import com.groupcall.common.UserManager;
import com.groupcall.common.Utils;
import com.groupcall.model.User;
import com.groupcall.network.app.AppPacket;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by tantn
 */
public class JoinRoomProcessor extends ProcessorBase {
    private static final String TAG = JoinRoomProcessor.class.getSimpleName();

    public void process(AppPacket appPacket) {
        try {
            JSONObject jsonObject = appPacket.getJsonData();
            String roomName = jsonObject.getString("roomName");
            String myName = jsonObject.getString("name");
            JSONArray arrayMember = jsonObject.getJSONArray("list");
            ArrayList<User> users = new ArrayList<>();
            for (int i = 0; i < arrayMember.length(); i++) {
                String name = arrayMember.getString(i);
                User user = new User();
                user.setName(name);
                user.setRoomName(roomName);
                UserManager.getInstance().add(user);
                users.add(user);
            }
            NotifyService.notifyJoinRoom(roomName, myName, users);
        } catch (Exception e) {
            Utils.log(TAG, e.getMessage());
        }
    }
}
