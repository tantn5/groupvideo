package com.groupcall.network.worker;

import com.groupcall.common.Common;
import com.groupcall.common.NotifyService;
import com.groupcall.common.UserManager;
import com.groupcall.common.Utils;
import com.groupcall.model.User;
import com.groupcall.network.app.AppPacket;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by tantn
 */
public class LeaveRoomProcessor extends ProcessorBase {
    private static final String TAG = LeaveRoomProcessor.class.getSimpleName();

    public void process(AppPacket appPacket) {
        try {
            JSONObject jsonObject = appPacket.getJsonData();
            String roomName = jsonObject.getString("roomName");
            String name = jsonObject.getString("name");
            NotifyService.notifyLeaveRoom(roomName, name);
        } catch (Exception e) {
            Utils.log(TAG, e.getMessage());
        }
    }
}
