package com.groupcall.network.worker;

import com.groupcall.common.NotifyService;
import com.groupcall.common.UserManager;
import com.groupcall.common.Utils;
import com.groupcall.model.User;
import com.groupcall.network.app.AppPacket;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by tantn
 */
public class NewMemberProcessor extends ProcessorBase {
    private static final String TAG = NewMemberProcessor.class.getSimpleName();

    public void process(AppPacket appPacket) {
        try {
            JSONObject jsonObject = appPacket.getJsonData();
            String roomName = jsonObject.getString("roomName");
            String name = jsonObject.getString("name");
            User user = new User();
            user.setName(name);
            user.setRoomName(roomName);
            UserManager.getInstance().add(user);
            NotifyService.notifyNewMember(user);
        } catch (Exception e) {
            Utils.log(TAG, e.getMessage());
        }
    }
}
