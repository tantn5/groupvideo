package com.groupcall.network.worker;

import com.groupcall.network.app.AppPacket;

/**
 * Created by tantn
 */
public abstract class ProcessorBase {
    public abstract void process(AppPacket appPacket);
}
