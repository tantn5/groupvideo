package com.groupcall.network.worker;

import com.groupcall.common.NotifyService;
import com.groupcall.common.UserManager;
import com.groupcall.common.Utils;
import com.groupcall.model.User;
import com.groupcall.network.app.AppPacket;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by tantn
 */
public class SdpAnswerProcessor extends ProcessorBase {
    private static final String TAG = SdpAnswerProcessor.class.getSimpleName();

    public void process(AppPacket appPacket) {
        try {
            JSONObject jsonObject = appPacket.getJsonData();
            String name = jsonObject.getString("name");
            String sdpAnswer = jsonObject.getString("sdpAnswer");
            NotifyService.notifySdpAnswer(name, sdpAnswer);
        } catch (Exception e) {
            Utils.log(TAG, e.getMessage());
        }
    }
}
