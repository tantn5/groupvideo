package com.webrtc;

import com.groupcall.common.Utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.DataChannel;
import org.webrtc.IceCandidate;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;

/**
 * Created by tantn on 4/29/2016.
 */
public class PCObserver implements PeerConnection.Observer {
    private static final String TAG = PCObserver.class.getSimpleName();

    private WebRtcListener webRTCListener;

    public PCObserver(WebRtcListener webRTCListener) {
        this.webRTCListener = webRTCListener;
    }

    @Override
    public void onSignalingChange(PeerConnection.SignalingState signalingState) {
        Utils.log(TAG, "PCObserver: onSignalingChange");
    }

    @Override
    public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
        Utils.log(TAG, "PCObserver: onIceConnectionChange");
    }

    @Override
    public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
        Utils.log(TAG, "PCObserver: onIceGatheringChange");
    }

    @Override
    public void onIceCandidate(IceCandidate iceCandidate) {
        Utils.log(TAG, "PCObserver: onIceCandidate=" + iceCandidate);

        //send cho peer con lai
        try {
            JSONObject obCandidate = new JSONObject();
            obCandidate.put("sdpMid", iceCandidate.sdpMid);
            obCandidate.put("sdpMLineIndex", iceCandidate.sdpMLineIndex);
            obCandidate.put("offer", iceCandidate.sdp);

            if (webRTCListener != null) {
                webRTCListener.onIceCandidateReceiver(obCandidate);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAddStream(MediaStream mediaStream) {
        Utils.log(TAG, "PCObserver: onAddStream");
        if (webRTCListener != null) {
            webRTCListener.onAddStream(mediaStream);
        }
    }

    @Override
    public void onRemoveStream(MediaStream mediaStream) {
        Utils.log(TAG, "PCObserver: onRemoveStream");
        if (webRTCListener != null) {
            webRTCListener.onRemoveStream(mediaStream);
        }
    }

    @Override
    public void onDataChannel(DataChannel dataChannel) {
        Utils.log(TAG, "PCObserver: onDataChannel");
    }

    @Override
    public void onRenegotiationNeeded() {
        Utils.log(TAG, "PCObserver: onRenegotiationNeeded");
    }

    @Override
    public void onIceConnectionReceivingChange(boolean b) {
        Utils.log(TAG, "PCObserver: onIceConnectionReceivingChange");
    }
}
