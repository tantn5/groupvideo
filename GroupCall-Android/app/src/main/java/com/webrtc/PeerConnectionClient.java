package com.webrtc;

import android.content.Context;
import android.media.AudioManager;

import com.groupcall.common.Common;
import com.groupcall.common.Utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.CameraEnumerationAndroid;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SessionDescription;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoCapturerAndroid;
import org.webrtc.VideoRenderer;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by tantn
 * Với user là mình thì chỉ create peer với mode send only
 * Với user khác thì create peer với mode recv only
 */

public class PeerConnectionClient {
    private static final String TAG = PeerConnectionClient.class.getSimpleName();
    public static String AUDIO_TRACK_ID = "ARDAMSa0";
    public static String VIDEO_TRACK_ID = "ARDAMSv0";
    public static String MEDIA_STREAM_LABEL = "ARDAMS";
    private static final String DTLS_SRTP_KEY_AGREEMENT_CONSTRAINT = "DtlsSrtpKeyAgreement";
    private static final String AUDIO_ECHO_CANCELLATION_CONSTRAINT = "googEchoCancellation";
    private static final String AUDIO_AUTO_GAIN_CONTROL_CONSTRAINT = "googAutoGainControl";
    private static final String AUDIO_HIGH_PASS_FILTER_CONSTRAINT = "googHighpassFilter";
    private static final String AUDIO_NOISE_SUPPRESSION_CONSTRAINT = "googNoiseSuppression";

    private static final String MAX_VIDEO_WIDTH_CONSTRAINT = "maxWidth";
    private static final String MIN_VIDEO_WIDTH_CONSTRAINT = "minWidth";
    private static final String MAX_VIDEO_HEIGHT_CONSTRAINT = "maxHeight";
    private static final String MIN_VIDEO_HEIGHT_CONSTRAINT = "minHeight";

    private Context context;
    private WebRtcListener webRTCListener;
    private AudioSource audioSource;
    private int audioOriginalMode;
    private VideoSource videoSource;
    private VideoCapturer videoCapturer;//to switch camera
    private VideoTrack videoTrack;

    public PeerConnection peerConnection;
    public PeerConnectionFactory peerConnectionFactory;

    public PeerConnectionClient() {
    }

    public void createPeerConnection(Context context, SurfaceViewRenderer renderer, WebRtcListener webRTCListener, boolean isSendOnly) {
        this.context = context;
        this.webRTCListener = webRTCListener;

        //init audio
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        am.setSpeakerphoneOn(true);
        audioOriginalMode = am.getMode();//luu lai mode cu~
        am.setMode(AudioManager.MODE_IN_COMMUNICATION);

        //init peer connection
        boolean isInitialize = PeerConnectionFactory.initializeAndroidGlobals(context, true, true, false);
        if (!isInitialize) {
            Utils.log(TAG, "init WebRTC unsuccessfully");
            return;
        }

        //create peerConnectionFactory
        peerConnectionFactory = new PeerConnectionFactory();

        MediaStream mediaStream = peerConnectionFactory.createLocalMediaStream(MEDIA_STREAM_LABEL + "_" + Common.me.getName());
        if (isSendOnly) {
            //create audio track
            audioSource = peerConnectionFactory.createAudioSource(getAudioConstraints());
            AudioTrack audioTrack = peerConnectionFactory.createAudioTrack(AUDIO_TRACK_ID, audioSource);
            audioTrack.setEnabled(true);

            //create video track
            String cameraName = CameraEnumerationAndroid.getNameOfBackFacingDevice();
            if (cameraName == null) {
                cameraName = CameraEnumerationAndroid.getNameOfBackFacingDevice();
            }
            videoCapturer = VideoCapturerAndroid.create(cameraName, null);
            MediaConstraints videoConstraints = getVideoConstraints(640, 480, 960, 720);
            videoSource = peerConnectionFactory.createVideoSource(videoCapturer, videoConstraints);
            videoTrack = peerConnectionFactory.createVideoTrack(VIDEO_TRACK_ID, videoSource);
            videoTrack.setEnabled(true);

            //create media stream
            mediaStream.addTrack(audioTrack);
            VideoRenderer videoRenderer = new VideoRenderer(renderer);
            videoTrack.addRenderer(videoRenderer);
            mediaStream.addTrack(videoTrack);
        }

        //init pccontraints
        MediaConstraints pcConstraints = getPCConstraints();

        //init PCObserver
        Utils.log(TAG, "init pcobserver");
        PCObserver pcObServer = new PCObserver(webRTCListener);

        Utils.log(TAG, "add media stream");
        PeerConnection.RTCConfiguration configuration = new PeerConnection.RTCConfiguration(new ArrayList<PeerConnection.IceServer>());
        peerConnection = peerConnectionFactory.createPeerConnection(configuration, pcConstraints, pcObServer);
        if (isSendOnly) {//chi send video moi add stream nay
            peerConnection.addStream(mediaStream);
        }

        Utils.log(TAG, "offer call");
        MediaConstraints sdpConstraints = getSdpConstraints();
        sdpConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveAudio", String.valueOf(!isSendOnly)));
        sdpConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveVideo", String.valueOf(!isSendOnly)));
        SDPObserver sdpObServer = new SDPObserver(this, false, webRTCListener);

        //create offer
        peerConnection.createOffer(sdpObServer, sdpConstraints);
    }

    public void endCall() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Utils.log(TAG, "clear peerConnection");
                if (peerConnection != null) {
                    peerConnection.dispose();
                    peerConnection = null;
                }
                Utils.log(TAG, "clear audioSource");
                if (audioSource != null) {
                    audioSource.dispose();
                    audioSource = null;
                }
                Utils.log(TAG, "clear videoSource");
                if (videoSource != null) {
                    videoSource.dispose();
                    videoSource = null;
                }
                Utils.log(TAG, "clear peerConnectionFactory");
//                if (peerConnectionFactory != null) {
//                    peerConnectionFactory.dispose();
//                    peerConnectionFactory = null;
//                }

                //close peer
                Utils.log(TAG, "clear stopInternalTracingCapture and shutdownInternalTracer");
                PeerConnectionFactory.stopInternalTracingCapture();
                PeerConnectionFactory.shutdownInternalTracer();

                Utils.log(TAG, "set audioOriginalMode");
                if (audioOriginalMode != 0) {
                    AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                    am.setMode(audioOriginalMode);
                }
            }
        }).start();
    }

    public void setRemoteDescription(JSONObject sdpOb) {
        try {
            Utils.log(TAG, "setRemoteDescription: sdpOb= " + sdpOb);
            String type = sdpOb.getString("type");
            String sdpStr = sdpOb.getString("offer");

            SessionDescription.Type typeDes = SessionDescription.Type.OFFER;
            if (type.equals("offer")) {
                typeDes = SessionDescription.Type.OFFER;
            } else if (type.equals("answer")) {
                typeDes = SessionDescription.Type.ANSWER;
            }
            SessionDescription sessionDescription = new SessionDescription(typeDes, sdpStr);
            peerConnection.setRemoteDescription(new SDPObserver(this, false, webRTCListener), sessionDescription);
        } catch (Exception e) {
            Utils.log(TAG, e.getMessage());
        }
    }

    public void addIceCandidate(String sdpMid, int sdpMLineIndex, String candidate) {
        peerConnection.addIceCandidate(new IceCandidate(sdpMid, sdpMLineIndex, candidate));
    }

    public MediaConstraints getAudioConstraints() {
        MediaConstraints audioConstraints = new MediaConstraints();
        audioConstraints.mandatory.add(new MediaConstraints.KeyValuePair("echoCancellation", "true"));//check lai can ko?
        audioConstraints.mandatory.add(new MediaConstraints.KeyValuePair(AUDIO_ECHO_CANCELLATION_CONSTRAINT, "true"));
        audioConstraints.mandatory.add(new MediaConstraints.KeyValuePair(AUDIO_NOISE_SUPPRESSION_CONSTRAINT, "true"));
        audioConstraints.mandatory.add(new MediaConstraints.KeyValuePair(AUDIO_AUTO_GAIN_CONTROL_CONSTRAINT, "true"));
        audioConstraints.mandatory.add(new MediaConstraints.KeyValuePair(AUDIO_HIGH_PASS_FILTER_CONSTRAINT, "true"));
        return audioConstraints;
    }

    private MediaConstraints getVideoConstraints(int minWidth, int minHeight, int maxWidth, int maxHeight) {
        MediaConstraints videoConstraints = new MediaConstraints();
        videoConstraints.mandatory.add(new MediaConstraints.KeyValuePair(MIN_VIDEO_WIDTH_CONSTRAINT, Integer.toString(minWidth)));
        videoConstraints.mandatory.add(new MediaConstraints.KeyValuePair(MAX_VIDEO_WIDTH_CONSTRAINT, Integer.toString(maxWidth)));
        videoConstraints.mandatory.add(new MediaConstraints.KeyValuePair(MIN_VIDEO_HEIGHT_CONSTRAINT, Integer.toString(minHeight)));
        videoConstraints.mandatory.add(new MediaConstraints.KeyValuePair(MAX_VIDEO_HEIGHT_CONSTRAINT, Integer.toString(maxHeight)));
        return videoConstraints;
    }

    public MediaConstraints getPCConstraints() {
        MediaConstraints pcConstraints = new MediaConstraints();
        pcConstraints.optional.add(new MediaConstraints.KeyValuePair(DTLS_SRTP_KEY_AGREEMENT_CONSTRAINT, "true"));//false = loopback
        return pcConstraints;
    }

    public MediaConstraints getSdpConstraints() {
        MediaConstraints sdpConstraints = new MediaConstraints();
        sdpConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true"));
        sdpConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true"));
        return sdpConstraints;
    }

    public LinkedList<PeerConnection.IceServer> getIceServes() {
        LinkedList<PeerConnection.IceServer> iceServers = new LinkedList<>();
        return iceServers;
    }
}
