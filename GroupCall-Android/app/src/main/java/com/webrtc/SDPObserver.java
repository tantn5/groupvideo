package com.webrtc;

import com.groupcall.common.Utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.MediaConstraints;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;

/**
 * Created by tantn on 4/29/2016.
 */
public class SDPObserver implements SdpObserver {
    private static final String TAG = SDPObserver.class.getSimpleName();

    private WebRtcListener webRTCListener;
    private boolean isLocal = false;
    private PeerConnectionClient peerConnectionClient;

    public SDPObserver(PeerConnectionClient peerConnectionClient, boolean isLocal, WebRtcListener webRTCListener) {
        this.peerConnectionClient = peerConnectionClient;
        this.isLocal = isLocal;
        this.webRTCListener = webRTCListener;
    }

    @Override
    public void onCreateSuccess(SessionDescription sessionDescription) {
        Utils.log(TAG, "SDPObserver: onCreateSuccess: sessionDescription=" + sessionDescription.description);
        if (peerConnectionClient.peerConnection != null) {
            peerConnectionClient.peerConnection.setLocalDescription(this, sessionDescription);
        }

        try {
            JSONObject jsonSdp = new JSONObject();
            if (sessionDescription.type == SessionDescription.Type.OFFER) {
                jsonSdp.put("type", "offer");
            } else if (sessionDescription.type == SessionDescription.Type.ANSWER) {
                jsonSdp.put("type", "answer");
            }
            jsonSdp.put("offer", sessionDescription.description);
            webRTCListener.onSdpReceiver(jsonSdp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSetSuccess() {
        Utils.log(TAG, "SDPObserver: onSetSuccess");

        //create answer
        if (!isLocal) {
            MediaConstraints sdpConstraints = peerConnectionClient.getSdpConstraints();
            if (peerConnectionClient.peerConnection != null) {
                peerConnectionClient.peerConnection.createAnswer(this, sdpConstraints);
            }
        }
    }

    @Override
    public void onCreateFailure(String s) {
        Utils.log(TAG, "SDPObserver: onCreateFailure");
    }

    @Override
    public void onSetFailure(String s) {
        Utils.log(TAG, "SDPObserver: onSetFailure");
    }
}
