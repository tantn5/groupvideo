package com.webrtc;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tantn on 5/3/2016.
 */
public class WebRTCCallInfo {
    public long from;
    public long to;
    public long userId;
    public long callId;
    public int status;
    public List<JSONObject> candidates = new ArrayList<>();
    public long startTime;
    public boolean isEndBeginStartView = false;
}
