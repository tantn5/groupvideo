package com.webrtc;

import org.json.JSONObject;
import org.webrtc.MediaStream;

/**
 * Created by tantn on 4/29/2016.
 */
public interface WebRtcListener {
    public void onIceCandidateReceiver(JSONObject iceCandidate);

    public void onSdpReceiver(JSONObject sdp);

    public void onAddStream(MediaStream stream);

    public void onRemoveStream(MediaStream stream);
}
