@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  GroupCall startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Add default JVM options here. You can also use JAVA_OPTS and GROUP_CALL_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS=

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto init

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto init

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:init
@rem Get command-line arguments, handling Windows variants

if not "%OS%" == "Windows_NT" goto win9xME_args
if "%@eval[2+2]" == "4" goto 4NT_args

:win9xME_args
@rem Slurp the command line arguments.
set CMD_LINE_ARGS=
set _SKIP=2

:win9xME_args_slurp
if "x%~1" == "x" goto execute

set CMD_LINE_ARGS=%*
goto execute

:4NT_args
@rem Get arguments from the 4NT Shell from JP Software
set CMD_LINE_ARGS=%$

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\GroupCall-1.0-SNAPSHOT.jar;%APP_HOME%\lib\netty-socketio-1.7.11.jar;%APP_HOME%\lib\netty-3.9.0.Final.jar;%APP_HOME%\lib\json-20160810.jar;%APP_HOME%\lib\kurento-client-6.6.1.jar;%APP_HOME%\lib\netty-transport-native-epoll-4.1.1.Final.jar;%APP_HOME%\lib\slf4j-api-1.7.21.jar;%APP_HOME%\lib\jackson-core-2.7.4.jar;%APP_HOME%\lib\jackson-databind-2.7.4.jar;%APP_HOME%\lib\kurento-jsonrpc-client-6.6.1.jar;%APP_HOME%\lib\kms-api-core-6.6.0.jar;%APP_HOME%\lib\kms-api-elements-6.6.0.jar;%APP_HOME%\lib\kms-api-filters-6.6.0.jar;%APP_HOME%\lib\jackson-annotations-2.7.0.jar;%APP_HOME%\lib\paranamer-2.8.jar;%APP_HOME%\lib\javax.inject-1.jar;%APP_HOME%\lib\kurento-commons-6.6.1.jar;%APP_HOME%\lib\httpclient-4.5.1.jar;%APP_HOME%\lib\fluent-hc-4.5.1.jar;%APP_HOME%\lib\netty-handler-proxy-4.1.5.Final.jar;%APP_HOME%\lib\gson-2.5.jar;%APP_HOME%\lib\guava-19.0.jar;%APP_HOME%\lib\httpcore-4.4.3.jar;%APP_HOME%\lib\commons-logging-1.2.jar;%APP_HOME%\lib\commons-codec-1.9.jar;%APP_HOME%\lib\netty-codec-socks-4.1.5.Final.jar;%APP_HOME%\lib\netty-handler-4.1.5.Final.jar;%APP_HOME%\lib\netty-transport-4.1.5.Final.jar;%APP_HOME%\lib\netty-codec-http-4.1.5.Final.jar;%APP_HOME%\lib\netty-codec-4.1.5.Final.jar;%APP_HOME%\lib\netty-buffer-4.1.5.Final.jar;%APP_HOME%\lib\netty-resolver-4.1.5.Final.jar;%APP_HOME%\lib\netty-common-4.1.5.Final.jar

@rem Execute GroupCall
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %GROUP_CALL_OPTS%  -classpath "%CLASSPATH%" server.Start %CMD_LINE_ARGS%

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable GROUP_CALL_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%GROUP_CALL_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
