package common;

/**
 * Created by tantn
 */
public class Utils {
    public static void log(String msg) {
        System.out.print("\n++++++++++" + msg);
    }

    public static void log(String tag, String msg) {
        System.out.print("\n++++++++++" + tag + ": " + msg);
    }
}
