package media;

import common.Constant;
import org.kurento.client.KurentoClient;
import org.kurento.client.MediaPipeline;

/**
 * Created by tantn
 */
public class KurentoManager {
    private static KurentoManager instance;
    private KurentoClient kurentoClient;

    public static KurentoManager getInstance() {
        if (instance == null) {
            instance = new KurentoManager();
        }
        return instance;
    }

    private KurentoManager() {
        kurentoClient = KurentoClient.create(Constant.kms_uri);
    }

    public MediaPipeline createMediaPipeLine(){
        return  kurentoClient.createMediaPipeline();
    }
}
