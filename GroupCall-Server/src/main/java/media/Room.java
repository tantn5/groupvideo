/*
 * (C) Copyright 2014 Kurento (http://kurento.org/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package media;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.PreDestroy;

import common.Utils;
import network.app.AppPacket;
import network.app.AppServiceType;
import org.jboss.netty.channel.Channel;
import org.kurento.client.Continuation;
import org.kurento.client.MediaPipeline;

/**
 * @author tantn
 */
public class Room implements Closeable {
    private static final String TAG = Room.class.getSimpleName();

    private final ConcurrentMap<String, User> members = new ConcurrentHashMap<String, User>();
    private final MediaPipeline pipeline;
    private final String name;

    public String getName() {
        return name;
    }

    public Room(String roomName, MediaPipeline pipeline) {
        this.name = roomName;
        this.pipeline = pipeline;
    }

    @PreDestroy
    private void shutdown() {
        this.close();
    }

    public User join(String userName, Channel channel) throws IOException {
        User user = new User(userName, this.name, channel, this.pipeline);
        AppPacket appPacketForward = new AppPacket(AppServiceType.NEW_MEMBER.getValue());
        appPacketForward.put("roomName", this.name);
        appPacketForward.put("name", userName);
        sendToOther(userName, appPacketForward);
        members.put(user.getName(), user);
        return user;
    }

    public void leave(User user) throws IOException {
        members.remove(user.getName());
        for (User u : members.values()) {
            u.cancelVideoFrom(user.getName());
        }
        //send ban tin leave den cac user khac
        AppPacket appPacket = new AppPacket(AppServiceType.LEAVE_ROOM.getValue());
        appPacket.put("name", user.getName());
        appPacket.put("roomName", getName());
        sendToOther(user.getRoomName(), appPacket);
        user.close();
    }

    //send ban tin den cac user khac name
    public void sendToOther(String name, AppPacket appPacket) {
        for (User user : members.values()) {
            if (!user.getName().equals(name)) {
                user.sendPacket(appPacket);
            }
        }
    }

    public Collection<User> getMembers() {
        return members.values();
    }

    public void close() {
        for (final User user : members.values()) {
            try {
                user.close();
            } catch (IOException e) {
                Utils.log(TAG, e.getMessage());
            }
        }

        members.clear();

        //giai phong pipeline
        pipeline.release(new Continuation<Void>() {
            public void onSuccess(Void result) throws Exception {
            }

            public void onError(Throwable cause) throws Exception {
            }
        });
    }

}
