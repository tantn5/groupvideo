/*
 * (C) Copyright 2014 Kurento (http://kurento.org/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package media;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.kurento.client.KurentoClient;

/**
 * @author tantn
 */
public class RoomManager {
    private static final String TAG = RoomManager.class.getSimpleName();
    private final ConcurrentMap<String, Room> rooms = new ConcurrentHashMap<String, Room>();

    private static RoomManager instance;

    public static RoomManager getInstance() {
        if (instance == null) {
            instance = new RoomManager();
        }
        return instance;
    }

    public Room get(String roomName) {
        Room room = rooms.get(roomName);

        //neu chua co room thi khoi tao room moi luon
        if (room == null) {
            room = new Room(roomName, KurentoManager.getInstance().createMediaPipeLine());
            rooms.put(roomName, room);
        }
        return room;
    }

    public void remove(Room room) {
        this.rooms.remove(room.getName());
        room.close();
    }

}
