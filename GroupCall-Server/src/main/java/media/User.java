/*
 * (C) Copyright 2014 Kurento (http://kurento.org/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package media;

import java.io.Closeable;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import common.Utils;
import network.app.AppPacket;
import network.app.AppServiceType;
import org.jboss.netty.channel.Channel;
import org.json.JSONObject;
import org.kurento.client.*;

import common.Constant;

/**
 * @author tantn
 */
public class User implements Closeable {
    private static final String TAG = User.class.getSimpleName();

    private final String name;
    private final Channel channel;

    private final MediaPipeline pipeline;

    private final String roomName;
    private final WebRtcEndpoint endpoint;
    private final ConcurrentMap<String, WebRtcEndpoint> otherEndpoints = new ConcurrentHashMap<String, WebRtcEndpoint>();

    public RecorderEndpoint recorder;

    public User(final String name, String roomName, final Channel chl,
                MediaPipeline pipeline) {

        this.pipeline = pipeline;
        this.name = name;
        this.channel = chl;
        this.roomName = roomName;
        this.endpoint = new WebRtcEndpoint.Builder(pipeline).build();
//        this.endpoint.setMaxVideoSendBandwidth(3000);
//        this.endpoint.setMinOutputBitrate(30);
//        this.endpoint.setMinVideoRecvBandwidth(1000);
//        this.endpoint.setMinVideoSendBandwidth(3000);

        //nhan dk candidate nao thi send luon den user nay
        this.endpoint.addIceCandidateFoundListener(new EventListener<IceCandidateFoundEvent>() {
            public void onEvent(IceCandidateFoundEvent event) {
                AppPacket appPacket = new AppPacket(AppServiceType.ICE_CANDIDATE.getValue());
                appPacket.put("name", name);
                JSONObject jsonCandidate = new JSONObject();
                jsonCandidate.put("sdpMid", event.getCandidate().getSdpMid());
                jsonCandidate.put("sdpMLineIndex", event.getCandidate().getSdpMLineIndex());
                jsonCandidate.put("candidate", event.getCandidate().getCandidate());
                appPacket.put("candidate", jsonCandidate);
                sendPacket(appPacket);
            }
        });

        if (name.length() > 0 && name.equals("phu")) {

            MediaProfileSpecType profile = MediaProfileSpecType.WEBM;

            recorder = new RecorderEndpoint.Builder(pipeline, Constant.RECORDER_FILE_PATH)
                    .withMediaProfile(profile).build();

            recorder.addRecordingListener(new EventListener<RecordingEvent>() {

                public void onEvent(RecordingEvent event) {
                    Utils.log("Recording Kurento...");
                }

            });

            recorder.addStoppedListener(new EventListener<StoppedEvent>() {

                public void onEvent(StoppedEvent event) {
                    Utils.log("Recording Kurento...Stop");
                }

            });

            recorder.addPausedListener(new EventListener<PausedEvent>() {

                public void onEvent(PausedEvent event) {
                    Utils.log("Recording Kurento...Paused");
                }

            });

            //this.endpoint.connect(this.endpoint);

            connectAccordingToProfile(this.endpoint, recorder, profile);

            Utils.log("Setup recording");

            recorder.record();
        }
    }

    public WebRtcEndpoint getEndPoint() {
        return endpoint;
    }

    public String getName() {
        return name;
    }

    public Channel getChannel() {
        return channel;
    }

    public String getRoomName() {
        return this.roomName;
    }

    public void offerVideo(User sender, JSONObject sdpOffer) throws IOException {
        String ipSdpAnswer = this.getEndpointForUser(sender).processOffer(sdpOffer.getString("offer"));
        AppPacket appPacketRes = new AppPacket(AppServiceType.SDP_ANSWER.getValue());
        appPacketRes.put("name", sender.getName());
        JSONObject jsonSdp = new JSONObject();
        jsonSdp.put("type", "answer");
        jsonSdp.put("offer", ipSdpAnswer);
        appPacketRes.put("sdpAnswer", jsonSdp);
        sendPacket(appPacketRes);
        Utils.log(TAG, "gather candidates");
        this.getEndpointForUser(sender).gatherCandidates();
    }

    private WebRtcEndpoint getEndpointForUser(final User sender) {
        if (sender.getName().equals(name)) {
            return endpoint;
        }

        //////////// Origin code /////////////////
//        WebRtcEndpoint otherEndpoint = otherEndpoints.get(sender.getName());
//        if (otherEndpoint == null) {
//            otherEndpoint = new WebRtcEndpoint.Builder(pipeline).build();
//
//            //nhan dk candidate thi send luon den user kia
//            otherEndpoint.addIceCandidateFoundListener(new EventListener<IceCandidateFoundEvent>() {
//
//                public void onEvent(IceCandidateFoundEvent event) {
//                    AppPacket appPacket = new AppPacket(AppServiceType.ICE_CANDIDATE.getValue());
//                    appPacket.put("name", sender.getName());
//                    JSONObject jsonCandidate = new JSONObject();
//                    jsonCandidate.put("sdpMid", event.getCandidate().getSdpMid());
//                    jsonCandidate.put("sdpMLineIndex", event.getCandidate().getSdpMLineIndex());
//                    jsonCandidate.put("candidate", event.getCandidate().getCandidate());
//                    appPacket.put("candidate", jsonCandidate);
//                    sendPacket(appPacket);
//                }
//            });
//
//            otherEndpoints.put(sender.getName(), otherEndpoint);
//        }
//
//        //connect den enpoint kia
//
//        sender.getEndPoint().connect(otherEndpoint);
        //////////////////////////////////////////////

        WebRtcEndpoint otherEndpoint = otherEndpoints.get(sender.getName());
        if (otherEndpoint == null) {
            otherEndpoint = connectToPlayerEndPoint(sender, Constant.RECORDER_FILE_PATH2);
            otherEndpoints.put(sender.getName(), otherEndpoint);
        }
        return otherEndpoint;
    }

    public WebRtcEndpoint connectToPlayerEndPoint (final User sender, String videoUrl) {

        URL url = null;
        URI uri = null;
        try {
            url = new URL(videoUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            uri = url.toURI();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        WebRtcEndpoint webRtcEndpoint = new WebRtcEndpoint.Builder(pipeline).build();
        final PlayerEndpoint playerEndpoint = new PlayerEndpoint.Builder(pipeline, videoUrl).build();

        playerEndpoint.connect(webRtcEndpoint);

        webRtcEndpoint.addIceCandidateFoundListener(new EventListener<IceCandidateFoundEvent>() {

            public void onEvent(IceCandidateFoundEvent event) {
                AppPacket appPacket = new AppPacket(AppServiceType.ICE_CANDIDATE.getValue());
                appPacket.put("name", sender.getName());
                JSONObject jsonCandidate = new JSONObject();
                jsonCandidate.put("sdpMid", event.getCandidate().getSdpMid());
                jsonCandidate.put("sdpMLineIndex", event.getCandidate().getSdpMLineIndex());
                jsonCandidate.put("candidate", event.getCandidate().getCandidate());
                appPacket.put("candidate", jsonCandidate);
                sendPacket(appPacket);
            }
        });

        webRtcEndpoint.addMediaStateChangedListener(new EventListener<MediaStateChangedEvent>() {
            public void onEvent(MediaStateChangedEvent event) {
                if (event.getNewState() == MediaState.CONNECTED) {
                    VideoInfo videoInfo = playerEndpoint.getVideoInfo();
                    Utils.log("Media player changed");
                }
            }
        });

        // 3. PlayEndpoint
        playerEndpoint.addErrorListener(new EventListener<ErrorEvent>() {
            public void onEvent(ErrorEvent event) {
                Utils.log("Media player error");
            }
        });

        playerEndpoint.addEndOfStreamListener(new EventListener<EndOfStreamEvent>() {
            public void onEvent(EndOfStreamEvent event) {
                Utils.log("Media player end");
            }
        });

        playerEndpoint.play();

        return webRtcEndpoint;
    }

    public void cancelVideoFrom(String senderName) {
        WebRtcEndpoint incoming = otherEndpoints.remove(senderName);
        incoming.release(new Continuation<Void>() {
            public void onSuccess(Void result) throws Exception {
            }

            public void onError(Throwable cause) throws Exception {
            }
        });
    }

    public void close() throws IOException {
        for (String remoteParticipantName : otherEndpoints.keySet()) {

            WebRtcEndpoint ep = this.otherEndpoints.get(remoteParticipantName);

            ep.release(new Continuation<Void>() {

                public void onSuccess(Void result) throws Exception {
                }

                public void onError(Throwable cause) throws Exception {
                }
            });
        }

        endpoint.release(new Continuation<Void>() {

            public void onSuccess(Void result) throws Exception {
            }

            public void onError(Throwable cause) throws Exception {
            }
        });
    }

    public void sendPacket(AppPacket appPacket) {
        if (channel != null && channel.isOpen()) {
            Utils.log(TAG, "send to " + getName() + ": " + appPacket.toString());
            channel.write(appPacket);
        }
    }

    public void addCandidate(IceCandidate candidate, String name) {
        if (this.name.compareTo(name) == 0) {
            endpoint.addIceCandidate(candidate);
        } else {
            WebRtcEndpoint webRtc = otherEndpoints.get(name);
            if (webRtc != null) {
                webRtc.addIceCandidate(candidate);
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof User)) {
            return false;
        }
        User other = (User) obj;
        boolean eq = name.equals(other.name);
        eq &= roomName.equals(other.roomName);
        return eq;
    }



    private void connectAccordingToProfile(WebRtcEndpoint webRtcEndpoint, RecorderEndpoint recorder,
                                           MediaProfileSpecType profile) {
        switch (profile) {
            case WEBM:
                webRtcEndpoint.connect(recorder, MediaType.AUDIO);
                webRtcEndpoint.connect(recorder, MediaType.VIDEO);
                break;
            case WEBM_AUDIO_ONLY:
                webRtcEndpoint.connect(recorder, MediaType.AUDIO);
                break;
            case WEBM_VIDEO_ONLY:
                webRtcEndpoint.connect(recorder, MediaType.VIDEO);
                break;
            case MP4:
                webRtcEndpoint.connect(recorder, MediaType.AUDIO);
                webRtcEndpoint.connect(recorder, MediaType.VIDEO);
                break;
            case JPEG_VIDEO_ONLY:
                webRtcEndpoint.connect(recorder, MediaType.AUDIO);
                break;
            case KURENTO_SPLIT_RECORDER:
                webRtcEndpoint.connect(recorder, MediaType.AUDIO);
                webRtcEndpoint.connect(recorder, MediaType.VIDEO);
                break;
            default:
                throw new UnsupportedOperationException("Unsupported profile for this tutorial: " + profile);
        }
    }


    public void stop() {
        if (recorder != null) {
            final CountDownLatch stoppedCountDown = new CountDownLatch(1);
            ListenerSubscription subscriptionId = recorder
                    .addStoppedListener(new EventListener<StoppedEvent>() {

                        public void onEvent(StoppedEvent event) {
                            stoppedCountDown.countDown();
                        }
                    });
            recorder.stop();
            try {
                if (!stoppedCountDown.await(5, TimeUnit.SECONDS)) {
                    Utils.log("Error waiting for recorder to stop");
                }
            } catch (InterruptedException e) {
                Utils.log("Exception while waiting for state change", e.toString());
            }
            recorder.removeStoppedListener(subscriptionId);
        }
    }
}
