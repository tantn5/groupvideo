package network.app;

import com.corundumstudio.socketio.SocketIOClient;
import org.jboss.netty.channel.Channel;

/**
 * Created by tantn
 */
public class AppJob {
    private AppPacket appPacket;
    private SocketIOClient client;//if websocket
    private Channel channel;//if socket
    private int type = TYPE_SOCKET;

    public static final int TYPE_SOCKET = 0;
    public static final int TYPE_WEBSOCKET = 1;

    public AppPacket getAppPacket() {
        return appPacket;
    }

    public void setAppPacket(AppPacket appPacket) {
        this.appPacket = appPacket;
    }

    public SocketIOClient getClient() {
        return client;
    }

    public void setClient(SocketIOClient client) {
        this.client = client;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
