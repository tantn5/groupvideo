package network.app;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tantn
 */
public enum AppServiceType {
    JOIN_ROOM(1),
    LEAVE_ROOM(2),
    SDP_OFFER(3),
    SDP_ANSWER(4),
    ICE_CANDIDATE(5),
    NEW_MEMBER(6);

    static final Map<Integer, AppServiceType> types = new HashMap<Integer, AppServiceType>();

    static {
        final AppServiceType[] all = AppServiceType.values();
        for (AppServiceType clientServiceType : all) {
            types.put(Integer.valueOf(clientServiceType.getValue()), clientServiceType);
        }
    }

    private final short value;

    public static AppServiceType getServiceType(short value) {
        return types.get((int) value);
    }

    private AppServiceType(final int value) {
        this.value = (short) value;
    }

    public short getValue() {
        return value;
    }
}

