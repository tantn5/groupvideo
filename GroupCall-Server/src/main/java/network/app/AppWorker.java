package network.app;

import common.Utils;
import network.worker.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tantn
 */
public class AppWorker {
    private static final String TAG = AppWorker.class.getSimpleName();
    private static final Map<Short, ProcessorBase> mapProcessor = new HashMap<Short, ProcessorBase>();

    public AppWorker() {
    }

    static {
        mapProcessor.put(AppServiceType.JOIN_ROOM.getValue(), new JoinRoomProcessor());
        mapProcessor.put(AppServiceType.LEAVE_ROOM.getValue(), new LeaveRoomProcessor());
        mapProcessor.put(AppServiceType.SDP_OFFER.getValue(), new OfferProcessor());
        mapProcessor.put(AppServiceType.ICE_CANDIDATE.getValue(), new IceCandidateProcessor());
    }

    public static void handler(AppJob appJob) {
        ProcessorBase processorBase = mapProcessor.get(appJob.getAppPacket().getService());
        if (processorBase != null) {
            processorBase.process(appJob);
        } else {
            Utils.log("Could not found processor for: " + appJob.getAppPacket().toString());
        }
    }
}
