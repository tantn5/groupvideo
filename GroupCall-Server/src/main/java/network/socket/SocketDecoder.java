package network.socket;

import com.corundumstudio.socketio.SocketIOClient;
import common.Utils;
import network.app.AppPacket;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.CorruptedFrameException;
import org.jboss.netty.handler.codec.frame.FrameDecoder;
import org.json.JSONObject;

/**
 * Created by tantn
 */
public class SocketDecoder extends FrameDecoder {
    private static final String TAG = SocketDecoder.class.getSimpleName();

    @Override
    protected Object decode(ChannelHandlerContext context, Channel channel, ChannelBuffer buffer) throws Exception {
        if (buffer.readableBytes() < 6) {
            return null;
        }
        //them dong nay de tranh bi overload ban tin
        buffer.markReaderIndex();

        byte[] magic = new byte[2];
        buffer.readBytes(magic);
        if (magic[0] != 'G' || magic[1] != 'C') {
            buffer.resetReaderIndex();
            return null;
//            throw new CorruptedFrameException("Invalid magic number: " + new String(magic));
        }
        int lenght = buffer.readShort();
        AppPacket appPacket = new AppPacket(buffer.readShort());

        // read data
        if (lenght > 0) {
            if (buffer.readableBytes() < lenght) {
                buffer.resetReaderIndex();
                return null;
            }
            byte[] data = new byte[lenght];
            buffer.readBytes(data);
            appPacket.setJsonData(new JSONObject(new String(data, "UTF-8")));
        }
        Utils.log(TAG, "receiver: " + appPacket.toString());
        return appPacket;
    }
}
