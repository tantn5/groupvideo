package network.socket;

import common.Utils;
import network.app.AppPacket;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;

/**
 * Created by tantn
 */
public class SocketEncoder extends OneToOneEncoder {
    private static final String TAG = SocketEncoder.class.getSimpleName();

    @Override
    protected Object encode(ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception {
        ChannelBuffer channelBuffer = ChannelBuffers.dynamicBuffer();
        AppPacket appPacket = (AppPacket) msg;
        channelBuffer.writeBytes(AppPacket.magic.getBytes());
        channelBuffer.writeShort(appPacket.getLenght());
        channelBuffer.writeShort(appPacket.getService());
        channelBuffer.writeBytes(appPacket.getData());
//        Utils.log(TAG, "send: " + appPacket.toString());
        return channelBuffer;
    }
}
