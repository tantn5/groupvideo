package network.socket;

import common.Utils;
import media.User;
import media.UserManager;
import network.app.AppJob;
import network.app.AppPacket;
import network.app.AppServiceType;
import network.app.AppWorker;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;

/**
 * Created by tantn
 */
public class SocketHandler extends SimpleChannelUpstreamHandler {
    private static final String TAG = SocketHandler.class.getSimpleName();

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e)
            throws Exception {
        Utils.log(TAG, "channelClosed");
        User user = UserManager.getInstance().getBySession(String.valueOf(ctx.getChannel().getId()));
        if (user != null) {
            if (user != null && user.getName().equals("phu") && user.recorder!=null) {
                user.stop();
            }
            AppPacket appPacket = new AppPacket(AppServiceType.LEAVE_ROOM.getValue());
            appPacket.put("name", user.getName());
            appPacket.put("roomName", user.getRoomName());
            AppJob appJob = new AppJob();
            appJob.setAppPacket(appPacket);
            appJob.setChannel(ctx.getChannel());
            appJob.setType(AppJob.TYPE_SOCKET);
            AppWorker.handler(appJob);
        }
    }

    @Override
    public void channelDisconnected(ChannelHandlerContext ctx,
                                    ChannelStateEvent e) throws Exception {
        Utils.log(TAG, "channelDisconnected");
    }

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e)
            throws Exception {
        Utils.log(TAG, "channelConnected");
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e)
            throws Exception {
        AppPacket appPacket = (AppPacket) e.getMessage();
        AppJob appJob = new AppJob();
        appJob.setAppPacket(appPacket);
        appJob.setChannel(ctx.getChannel());
        appJob.setType(AppJob.TYPE_SOCKET);
        AppWorker.handler(appJob);
    }
}
