package network.socket;

import common.Utils;
import network.websocket.WebSocketListener;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * Created by tantn
 */
public class SocketListener {
    private static final String TAG = SocketListener.class.getSimpleName();
    private ServerBootstrap bootstrap;
    private static SocketListener instance;

    public static SocketListener getInstance() {
        if (instance == null) {
            instance = new SocketListener();
        }
        return instance;
    }

    public void start(int port) {
        bootstrap = new ServerBootstrap(
                new NioServerSocketChannelFactory(
                        Executors.newCachedThreadPool(),
                        Executors.newCachedThreadPool()));

        bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
            public ChannelPipeline getPipeline() throws Exception {
                ChannelPipeline pipeline = Channels.pipeline();
                pipeline.addLast(SocketDecoder.class.getSimpleName(), new SocketDecoder());
                pipeline.addLast(SocketEncoder.class.getSimpleName(), new SocketEncoder());
                pipeline.addLast(SocketHandler.class.getSimpleName(), new SocketHandler());
                return pipeline;
            }
        });
        bootstrap.bind(new InetSocketAddress(port));

        Utils.log(TAG, "Socket is listening on " + port);
    }

    public void stop() {
        bootstrap.releaseExternalResources();
        Utils.log(TAG, "Socket has been stopped!");
    }
}
