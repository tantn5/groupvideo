package network.websocket;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import common.Utils;
import network.app.AppJob;
import network.app.AppPacket;
import network.app.AppWorker;
import org.json.JSONObject;

import java.io.InputStream;

/**
 * Created by tantn
 */
public class WebSocketListener {
    private static WebSocketListener instance;

    public static WebSocketListener getInstance() {
        if (instance == null) {
            instance = new WebSocketListener();
        }
        return instance;
    }

    //server listener at port
    public void start(int port) {
        InputStream inputStream = null;
        try {
            //configuration
            Configuration configuration = new Configuration();
            configuration.setHostname("0.0.0.0");//global
            configuration.setPort(port);

            //create listener
            SocketIOServer socketIOServer = new SocketIOServer(configuration);
            socketIOServer.addConnectListener(new ConnectListener() {
                public void onConnect(SocketIOClient client) {
                    Utils.log("Client connect: " + client.getSessionId());
                }
            });
            socketIOServer.addDisconnectListener(new DisconnectListener() {
                public void onDisconnect(SocketIOClient client) {
                    Utils.log("Client diconnect: " + client.getSessionId());
                }
            });
            socketIOServer.addEventListener("message", WSMessageObj.class, new DataListener<WSMessageObj>() {
                public void onData(SocketIOClient client, WSMessageObj data, AckRequest ackSender) throws Exception {
                    Utils.log("Receiver message: " + data.service + " | " + data.body);
                    AppJob appJob = new AppJob();
                    AppPacket appPacket = new AppPacket(data.service, new JSONObject(data.body));
                    appJob.setType(AppJob.TYPE_SOCKET);
                    appJob.setClient(client);
                    AppWorker.handler(appJob);
                }
            });
            socketIOServer.start();
            Utils.log("WebSocket listener at port: " + port);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
