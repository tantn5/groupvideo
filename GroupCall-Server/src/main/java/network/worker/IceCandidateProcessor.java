package network.worker;

import common.Utils;
import media.UserManager;
import media.User;
import network.app.AppJob;
import org.json.JSONObject;
import org.kurento.client.IceCandidate;

/**
 * Created by tantn
 */
public class IceCandidateProcessor extends ProcessorBase {
    private static final String TAG = IceCandidateProcessor.class.getSimpleName();

    public void process(AppJob appJob) {
        try {
            User user = UserManager.getInstance().getBySession(String.valueOf(appJob.getChannel().getId()));
            JSONObject jsonObject = appJob.getAppPacket().getJsonData();
            JSONObject jsonCandidate = jsonObject.getJSONObject("candidate");

            if (user != null) {
                IceCandidate cand = new IceCandidate(
                        jsonCandidate.getString("offer"),
                        jsonCandidate.getString("sdpMid"),
                        jsonCandidate.getInt("sdpMLineIndex"));
                user.addCandidate(cand, jsonObject.getString("name"));
            }
        } catch (Exception e) {
            Utils.log(TAG, e.getMessage());
        }
    }
}
