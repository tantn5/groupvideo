package network.worker;

import common.Utils;
import media.Room;
import media.RoomManager;
import media.UserManager;
import media.User;
import network.app.AppJob;
import network.app.AppPacket;
import network.app.AppServiceType;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by tantn
 */
public class JoinRoomProcessor extends ProcessorBase {
    private static final String TAG = JoinRoomProcessor.class.getSimpleName();

    public void process(AppJob appJob) {
        try {
            JSONObject jsonObject = appJob.getAppPacket().getJsonData();
            String roomName = jsonObject.getString("roomName");
            String name = jsonObject.getString("name");

            Room room = RoomManager.getInstance().get(roomName);
            User user = room.join(name, appJob.getChannel());
            UserManager.getInstance().add(user);

            //response
            JSONArray arrExistMember = new JSONArray();
            for (User u : room.getMembers()) {
                if (!u.equals(user)) {
                    arrExistMember.put(u.getName());
                }
            }
            AppPacket appPacket = new AppPacket(AppServiceType.JOIN_ROOM.getValue());
            appPacket.put("name", name);
            appPacket.put("roomName", roomName);
            appPacket.put("list", arrExistMember);
            user.sendPacket(appPacket);
        } catch (Exception e) {
            Utils.log(e.getMessage());
        }
    }
}
