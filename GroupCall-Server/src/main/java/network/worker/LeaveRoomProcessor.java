package network.worker;

import common.Utils;
import media.Room;
import media.RoomManager;
import media.UserManager;
import media.User;
import network.app.AppJob;
import network.app.AppPacket;
import network.app.AppServiceType;

/**
 * Created by tantn
 */
public class LeaveRoomProcessor extends ProcessorBase {
    private static final String TAG = LeaveRoomProcessor.class.getSimpleName();

    public void process(AppJob appJob) {
        try {
            User user = UserManager.getInstance().getBySession(String.valueOf(appJob.getChannel().getId()));
            Room room = RoomManager.getInstance().get(user.getRoomName());
            room.leave(user);
            if (room.getMembers().isEmpty()) {
                RoomManager.getInstance().remove(room);
            }
            AppPacket appPacketRes = new AppPacket(AppServiceType.LEAVE_ROOM.getValue());
            appPacketRes.put("r", 0);//sucess
            user.sendPacket(appPacketRes);
        } catch (Exception e) {
            Utils.log(TAG, e.getMessage());
        }
    }
}
