package network.worker;

import common.Utils;
import media.UserManager;
import media.User;
import network.app.AppJob;
import org.json.JSONObject;

/**
 * Created by tantn
 */
public class OfferProcessor extends ProcessorBase {

    private static final String TAG = OfferProcessor.class.getSimpleName();

    public void process(AppJob appJob) {
        try {
            User user = UserManager.getInstance().getBySession(String.valueOf(appJob.getChannel().getId()));
            JSONObject jsonObject = appJob.getAppPacket().getJsonData();
            String name = jsonObject.getString("name");
            User offerUser = UserManager.getInstance().get(name);
            JSONObject sdpOffer = jsonObject.getJSONObject("sdpOffer");
            user.offerVideo(offerUser, sdpOffer);
        } catch (Exception e) {
            Utils.log(TAG, e.getMessage());
        }
    }
}
