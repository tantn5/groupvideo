package network.worker;

import network.app.AppJob;

/**
 * Created by tantn
 */
public abstract class ProcessorBase {
    public abstract void process(AppJob appJob);
}
