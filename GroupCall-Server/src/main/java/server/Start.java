package server;

import common.Constant;
import common.Utils;
import network.socket.SocketListener;
import network.websocket.WebSocketListener;

/**
 * Created by tantn
 */
public class Start {
    public static void main(String[] args) {
        Utils.log("Start Application Server");

        //start websocket listener at app_port
        //WebSocketListener.getInstance().start(Constant.app_port);

        //start socket
        SocketListener.getInstance().start(Constant.app_port);
    }
}
