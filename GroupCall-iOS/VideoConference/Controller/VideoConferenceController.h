//
//  VideoConferenceController.h
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoConferenceVCHandler.h"

@interface VideoConferenceController : UIViewController <NetworkHandlerDelegate>


@property (weak, nonatomic) IBOutlet UIView *joinRoomView;

@property (weak, nonatomic) IBOutlet UIButton *joinRoomBtn;

@property (weak, nonatomic) IBOutlet UILabel *connectionLb;

@property (weak, nonatomic) IBOutlet UIView *roomView;

@property (weak, nonatomic) IBOutlet UIView *roomControlView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *leaveRoomBtn;

@property (weak, nonatomic) IBOutlet UILabel *totalMemberLb;

@property (strong, nonatomic) NSMutableArray *listVideoViews;

@property (strong, nonatomic) VideoConferenceVCHandler *handler;

- (void) showHUD;

- (void) hideHUD;

- (void) showJoinRoomUI;
- (IBAction)joinRoomClicked:(id)sender;

- (IBAction)leaveRoomClicked:(id)sender;


- (void) showConferenceUI;

- (void) updateConferenceUI: (NSArray *) listStreams;

- (void) audioRoute;

@end
