//
//  VideoConferenceController.m
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "VideoConferenceController.h"
#import "RTCEAGLVideoView.h"
#import "RTCMediaStream.h"
#import "RTCVideoTrack.h"


@interface VideoConferenceController () <RTCEAGLVideoViewDelegate>

@end

@implementation VideoConferenceController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = @"Demo video conference";
    
    //
    
    self.handler = [[VideoConferenceVCHandler alloc] init];
    self.handler.viewController = self;
    
    //
    CGRect frame = self.scrollView.frame;
    frame.origin.y = self.roomControlView.frame.size.height;
    frame.origin.x = 0;
    frame.size.width = [Manager scrWi];
    frame.size.height = [Manager scrHi] - 64 - frame.origin.y;
    
    self.scrollView.frame = frame;
    self.scrollView.backgroundColor = [UIColor grayColor];
    
    //
    [self.handler setupPrepareJoinRoomPhase];
    
    //
    [[Manager instance] connectToSignalingServer];    
    ((NetworkHandler *)[Manager instance].tcpClient.handler).delegate = self;
    
    //
    self.listVideoViews = [[NSMutableArray alloc] init];
}

- (void) showHUD {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void) hideHUD {
    [MBProgressHUD hideHUDForView:self.view animated:NO];
}

- (void) showJoinRoomUI {
    self.joinRoomView.hidden = NO;
    self.roomView.hidden = YES;
    for (int i=0; i<self.listVideoViews.count; i++) {
        UIView *itemVideo = self.listVideoViews[i];
        [itemVideo removeFromSuperview];
    }
}

- (IBAction)joinRoomClicked:(id)sender {
    [self.handler joinRoom];
}

- (IBAction)leaveRoomClicked:(id)sender {
    [self.handler leaveRoom];
}

- (void) showConferenceUI {
    self.joinRoomView.hidden = YES;
    self.roomView.hidden = NO;
}

- (void) updateConferenceUI: (NSArray *) listStreams {
    
    self.totalMemberLb.text = [NSString stringWithFormat:@"Total member: %d", listStreams.count];
    for (int i=0; i<self.listVideoViews.count; i++) {
        UIView *itemVideo = self.listVideoViews[i];
        [itemVideo removeFromSuperview];
    }
    
    [self.listVideoViews removeAllObjects];
    
    int y = 0;
    for (int i=0; i<listStreams.count; i++) {
        RTCMediaStream *stream = listStreams[i];
        for (int j=0; j<stream.videoTracks.count; j++) {
            RTCVideoTrack *videoTrack = stream.videoTracks[j];
            RTCEAGLVideoView* renderView = [self getRTCEAGLViewFrom:videoTrack];
            [self.listVideoViews addObject:renderView];
            renderView.frame = CGRectMake(0, y, [Manager scrWi], 500);
            [self.scrollView addSubview:renderView];
            y += 500;
        }
    }
    [self.scrollView setContentSize:CGSizeMake([Manager scrWi], 500*self.listVideoViews.count)];
    
    dispatch_async(dispatch_get_main_queue(), ^{        
        [self audioRoute];
    });
}

- (RTCEAGLVideoView*) getRTCEAGLViewFrom: (RTCVideoTrack *)videoTrack {
    RTCEAGLVideoView * view = [[RTCEAGLVideoView alloc] init];
    view.delegate = self;
    [videoTrack addRenderer:view];
    return view;
}

// Network handler delegate

- (void) onConnected {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateStatusSocketNetworkConnection];
    });
}

- (void) onDisconnected {
    dispatch_async(dispatch_get_main_queue(), ^{        
        [self updateStatusSocketNetworkConnection];
    });
}

- (void) onConnectToServer {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateStatusSocketNetworkConnection];
    });
}

// Update status network connection socket

- (void) updateStatusSocketNetworkConnection {
    switch ([((NetworkHandler *)[Manager instance].tcpClient.handler) getNetworkConnectionStatus]) {
        case SocketConnectionStatus_Connecting:
            self.connectionLb.text = @"Socket connection: Connecting...";
            break;
        case SocketConnectionStatus_Connected:
            self.connectionLb.text = @"Socket connection: Connected!";
            break;
        case SocketConnectionStatus_DisConnected:
            self.connectionLb.text = @"Socket connection: DisConnected!";
            break;
        default:
            break;
    }
}

// Delegate RTCEAGLVideoView

- (void)videoView:(RTCEAGLVideoView*)videoView didChangeVideoSize:(CGSize)size {
//    CGRect frame = videoView.frame;
//    frame.size.height = 200;
//    frame.size.width = 200.0 * (size.width/size.height);
//    frame.origin.x = ([Manager scrWi] - frame.size.width)/2.0;
//    videoView.frame = frame;
}

- (void) audioRoute {
    BOOL isSpeak = YES;
    
    if ([self isHeadsetPluggedIn]) {
        isSpeak = NO;
    }
    else {
        isSpeak = YES;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if([UIDevice currentDevice].systemVersion.floatValue >= 6.0) {
            if (isSpeak) {
                NSError * error;
                BOOL ok = [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
                if (ok) {
                    NSLog(@"Speak ok");
                }
                else {
                    NSLog(@"Speak not ok - %@", error);
                }
            } else {
                BOOL ok = [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:nil];
                if (ok) {
                }
            }
        } else {
            UInt32 route;
            if (isSpeak) {
                route = kAudioSessionOverrideAudioRoute_Speaker;
            } else {
                route = kAudioSessionOverrideAudioRoute_None;
            }
            AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute, sizeof(route), &route);
        }
    });
}

- (BOOL)isHeadsetPluggedIn {
    AVAudioSessionRouteDescription* route = [[AVAudioSession sharedInstance] currentRoute];
    for (AVAudioSessionPortDescription* desc in [route outputs]) {
        if ([[desc portType] isEqualToString:AVAudioSessionPortHeadphones])
            return YES;
    }
    return NO;
}

@end
