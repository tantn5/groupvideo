//
//  VideoConferenceVCHandler.h
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Room.h"

@class VideoConferenceController;

@interface VideoConferenceVCHandler : NSObject <RoomDelegate>

@property (weak, nonatomic) VideoConferenceController *viewController;

@property (weak, nonatomic) Room * room;

- (void) joinRoom;

- (void) leaveRoom;

- (void) setupPrepareJoinRoomPhase;

- (void) setupShowRoomPhase;

@end
