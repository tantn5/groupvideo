//
//  VideoConferenceVCHandler.m
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import "VideoConferenceVCHandler.h"
#import "RoomManager.h"
#import "VideoConferenceController.h"
#import "RTCCenter.h"
#import "PeerConnection.h"

@implementation VideoConferenceVCHandler

- (id) init {
    self = [super init];
    if (self) {
        self.room = [[RoomManager instance] createRoom:[[RTCCenter instance] getRoomId] roomDelegate:self];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillTerminate:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        
    }
    return self;
    
}

- (void) applicationWillTerminate: (UIApplication *) application{
}

- (void) joinRoom {
    
    [self.viewController showHUD];
    
    [self.room joinRoom];
    
}

- (void) leaveRoom {
    
    [self.viewController showHUD];
    
    [self.room leaveRoom];
    
}

- (void) setupPrepareJoinRoomPhase {
    
    [self.viewController showJoinRoomUI];

}

- (void) setupShowRoomPhase {
    
    [self.viewController showConferenceUI];
    
}

// Room delegate

- (void) joinRoomResponse: (NSDictionary *) info {
    //dispatch_async(dispatch_get_main_queue(), ^{
        [self.viewController hideHUD];
        [self.viewController showConferenceUI];
    //});
}

- (void) leaveRoomResponse: (NSDictionary *) info {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.viewController hideHUD];
        
        NSString *userLeave = info[@"name"];
        if ([userLeave isEqualToString:[[RTCCenter instance] getMyPeerConnectionId]]) {
            [self.viewController showJoinRoomUI];
            self.room = [[RoomManager instance] createRoom:[[RTCCenter instance] getRoomId] roomDelegate:self];
        }
    });
}

- (void) newMemberJoinRoom: (NSDictionary *) info {
    dispatch_async(dispatch_get_main_queue(), ^{
    });
}

- (void) receivedStream: (RTCMediaStream *) newStream {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self addAllVideoView];
    });
}

- (void) removedStream: (RTCMediaStream *) stream {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self addAllVideoView];
    });
}

- (void) addAllVideoView {
    NSMutableArray *list = [[NSMutableArray alloc] init];
    for (int i=0; i<self.room.peerConnections.allValues.count; i++) {
        RTCMediaStream *stream = ((PeerConnection *)self.room.peerConnections.allValues[i]).stream;
        if (stream) {
            [list addObject:stream];
        }
    }
    [self.viewController updateConferenceUI:list];
}

- (void) dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
}
@end
