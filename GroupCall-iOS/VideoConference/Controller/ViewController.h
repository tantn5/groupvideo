//
//  ViewController.h
//  TestUsageGoogleWebRTC
//
//  Created by Dau Ngoc Huy on 1/20/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebRTC/RTCPeerConnectionFactory.h>
#import <WebRTC/RTCPeerConnection.h>
#import <WebRTC/RTCVideoTrack.h>
#import <WebRTC/RTCAVFoundationVideoSource.h>
#import "App_RTCPeerConnectionDelegate.h"
#import "App_RTCSessionDescriptionDelegate.h"
#import "TcpClient.h"
#import "NetworkHandler.h"

@interface ViewController : UIViewController <UIGestureRecognizerDelegate, RTCEAGLVideoViewDelegate> {
    App_RTCPeerConnectionDelegate* app_RTCPeerConnectionDelegate;
    App_RTCSessionDescriptionDelegate* app_RTCSessionDescriptionDelegate;
    RTCPeerConnectionFactory *pcFactory;
    
    //signaling
    dispatch_queue_t queueForSlaveConnection;
    
    //size
    NSString* minWidth;
    NSString* minHeight;
    //
    NSString* maxWidth;
    NSString* maxHeight;
}

@property (weak, nonatomic) IBOutlet UITextField *txtRoomId;


- (IBAction)join:(id)sender;


@property (nonatomic) BOOL callStarted;
@property (weak, nonatomic) IBOutlet UITextField *txtMcuAddr;
@property (weak, nonatomic) IBOutlet UILabel *txtStatus;


@property (strong, nonatomic) RTCVideoTrack *videoTrack;

@property (strong, nonatomic) RTCEAGLVideoView* renderViewLocal;

- (void)startCall:(BOOL)isCaller;

- (RTCMediaConstraints *)mediaStreamConstraintsPeerConnection;

- (IBAction)startCallAsCaller:(id)sender;

@end

