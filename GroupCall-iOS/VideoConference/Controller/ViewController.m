//
//  ViewController.m
//  TestUsageGoogleWebRTC
//
//  Created by Dau Ngoc Huy on 1/20/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import "ViewController.h"
#import <WebRTC/RTCLogging.h>
#import <WebRTC/RTCICEServer.h>
#import <WebRTC/RTCMediaConstraints.h>
#import <WebRTC/RTCPeerConnectionInterface.h>
#import <WebRTC/RTCPair.h>
#import <WebRTC/RTCMediaStream.h>
#import <WebRTC/RTCAudioTrack.h>
#import <WebRTC/RTCVideoCapturer.h>
#import <AVFoundation/AVFoundation.h>
#import "Manager.h"
#import "IceServerUtil.h"

@implementation ViewController

// Example: src/webrtc/examples/objc/AppRTCDemo/

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [NSUserDefaults standardDefaults] setObject:dic forKey:@"DicKey"];
   NSString* serverAddr = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverAddr"];
    if (!serverAddr) {
        serverAddr = self.txtMcuAddr.text;
    }else{
        self.txtMcuAddr.text = serverAddr;
    }
    
    BOOL speedDevice = [Manager isSpeedDevice:[Manager machineName]];
    //
    if(speedDevice){
        minWidth = @"352";
        minHeight = @"288";
    }else{
        minWidth = @"352";
        minHeight = @"288";
    }
    //
    if(speedDevice){
        maxWidth = @"352";
        maxHeight = @"288";
    }else{
        maxWidth = @"352";
        maxHeight = @"288";
    }
    
    [Manager instance].viewController = self;
    self.callStarted = NO;
    
    [self connectToSignalingServer:serverAddr];
    
    CGFloat sWidth = [[UIScreen mainScreen] bounds].size.width;
    CGFloat sHeight = [[UIScreen mainScreen] bounds].size.height;
    NSLog(@"Screen size: %f x %f", sWidth, sHeight);
    
    NSArray* iceServers = [IceServerUtil getIceServers];
    NSLog(@"iceServers: %@", iceServers);
}

- (IBAction)startCallAsCaller:(id)sender {
    [self startCall:YES];
    
    [[NSUserDefaults standardUserDefaults] setObject:self.txtMcuAddr.text forKey:@"serverAddr"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)connectToSignalingServer:(NSString*)ip {
    queueForSlaveConnection = dispatch_queue_create("queue_slave_connection", NULL);
    
    NetworkHandler* handler = [[NetworkHandler alloc] init];
    
//    [Manager instance].tcpClient = [[TcpClient alloc] initWithRemoteHost:@"192.168.1.104" port:6695];
    [Manager instance].tcpClient = [[TcpClient alloc] initWithRemoteHost:@"nat2.azstack.com" port:6695];
    [Manager instance].tcpClient.ssl = NO;
    [Manager instance].tcpClient.sslServerName = @"bomchat.com";
//    [Manager instance].tcpClient = [[TcpClient alloc] initWithRemoteHost:@"bomchat.com" port:6695];
    [Manager instance].tcpClient.autoReconnect = YES;
    [Manager instance].tcpClient.handler = handler;
    [[Manager instance].tcpClient createSocketThread:queueForSlaveConnection];
}

//ham nay chi goi 1 lan dau tien
- (void)initAndCreatePeerConnectionFactory {
    [RTCPeerConnectionFactory initializeSSL];
    //The RTCPeerConnectionFactory object needs to be alive for the entire duration of your WebRTC session
    pcFactory = [[RTCPeerConnectionFactory alloc] init];
    
//    RTCSetMinDebugLogLevel(kRTCLoggingSeverityVerbose);
}

- (void)startCall:(BOOL)isCaller {
    self.callStarted = YES;
    
    [self initAndCreatePeerConnectionFactory];//ham nay chi goi 1 lan
    
    //1. Create peer connection.
    RTCConfiguration* config = [[RTCConfiguration alloc] init];
    config.iceServers = [IceServerUtil getIceServers];
    app_RTCPeerConnectionDelegate = [[App_RTCPeerConnectionDelegate alloc] init];//2. khai bao cac delegate
    [Manager instance].peerConnection = [pcFactory peerConnectionWithConfiguration:config
                                                                       constraints:[self mediaStreamConstraintsPeerConnection]
                                                                          delegate:app_RTCPeerConnectionDelegate];
    
    //3. Stream
    NSString* mediaStreamID = [NSString stringWithFormat:@"someUniqueStreamLabel_%d", [Manager instance].myUser.userId];
    RTCMediaStream *localStream = [pcFactory mediaStreamWithLabel:mediaStreamID];
    
    //add video to localStream
    AVCaptureDevice *device;
    AVCaptureDevicePosition camera = AVCaptureDevicePositionFront;
//    AVCaptureDevicePosition camera = AVCaptureDevicePositionBack;
    for (AVCaptureDevice *captureDevice in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] ) {
        if (captureDevice.position == camera) {
            device = captureDevice;
            break;
        }
    }
    // Create a video track and add it to the media stream
    if (device) {
        NSString* trackID = [NSString stringWithFormat:@"video_%d", [Manager instance].myUser.userId];
        
        RTCVideoCapturer *capturer = [RTCVideoCapturer capturerWithDeviceName:device.localizedName];//device.localizedName
        RTCVideoSource *videoSource = [pcFactory videoSourceWithCapturer:capturer constraints:[self mediaStreamConstraintsLocalVideo]];
        RTCVideoTrack *videoTrack = [pcFactory videoTrackWithID:trackID source:videoSource];
        self.videoTrack = videoTrack;
        
        [localStream addVideoTrack:videoTrack];
        
        //hien thi local video
        [self showLocalVideo:videoTrack];
    }
    
    //add audio to localStream
    RTCAudioTrack *audioTrack = [pcFactory audioTrackWithID:@"audio0"];
    [localStream addAudioTrack:audioTrack];
    
    //add localStream to PC
    [[Manager instance].peerConnection addStream:localStream];
    
    //4. Create offer/answer
    if (isCaller) {
        RTCMediaConstraints *constraints2 = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:
                                             @[
                                               [[RTCPair alloc] initWithKey:@"OfferToReceiveAudio" value:@"true"],
                                               [[RTCPair alloc] initWithKey:@"OfferToReceiveVideo" value:@"true"]
                                               ]
                                                                                  optionalConstraints: nil];
        
        app_RTCSessionDescriptionDelegate = [[App_RTCSessionDescriptionDelegate alloc] init];
        [[Manager instance].peerConnection createOfferWithDelegate:app_RTCSessionDescriptionDelegate constraints:constraints2];
    }
}

- (void)showLocalVideo:(RTCVideoTrack *)videoTrack {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.renderViewLocal = [[RTCEAGLVideoView alloc] initWithFrame:CGRectMake(200, 200, 100, 100)];
        self.renderViewLocal.delegate = self;
        
        NSLog(@"\n created renderViewLocal=%@ \n", self.renderViewLocal);
        
        [videoTrack addRenderer:self.renderViewLocal];
        NSLog(@"\n add renderViewLocal \n");
        [[Manager instance].viewController.view addSubview:self.renderViewLocal];
    });
}

//self.renderViewLocal.delegate
- (void)videoView:(RTCEAGLVideoView*)videoView didChangeVideoSize:(CGSize)size {
    NSLog(@"+++++++++++++++++++++++++++++ local real video size: %f x %f", size.width, size.height);
    //[[Manager instance].viewController.view addSubview:self.renderViewLocal];
    
    CGFloat sWidth = [[UIScreen mainScreen] bounds].size.width;//414.000000 x 736.000000
    CGFloat sHeight = [[UIScreen mainScreen] bounds].size.height;
    dispatch_async(dispatch_get_main_queue(), ^{
        CGRect localFrame = CGRectMake(sWidth - 150, sHeight - 180, 150, 180);
        self.renderViewLocal.frame = localFrame;
    });
}

//- (RTCVideoTrack*)createLocalVideoTrack {
//    RTCMediaConstraints *mediaConstraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:nil optionalConstraints:nil];
//    RTCAVFoundationVideoSource *source = [[RTCAVFoundationVideoSource alloc] initWithFactory:pcFactory constraints:mediaConstraints];
//    RTCVideoTrack* localVideoTrack1 = [[RTCVideoTrack alloc] initWithFactory:pcFactory source:source trackId:@"ARDAMSv0"];
//    return localVideoTrack1;
//}

- (RTCMediaConstraints *)mediaStreamConstraintsLocalVideo {
    //
    RTCPair *maxWidth1 = [[RTCPair alloc] initWithKey:@"maxWidth" value:maxWidth];
    RTCPair *maxHeight1 = [[RTCPair alloc] initWithKey:@"maxHeight" value:maxHeight];
    //
    RTCPair *minWidth1 = [[RTCPair alloc] initWithKey:@"minWidth" value:minWidth];
    RTCPair *minHeight1 = [[RTCPair alloc] initWithKey:@"minHeight" value:minHeight];
    //
    NSArray *videoConstraints = @[maxWidth1, maxHeight1, minWidth1, minHeight1];
    return [[RTCMediaConstraints alloc] initWithMandatoryConstraints:videoConstraints optionalConstraints:nil];
}

- (RTCMediaConstraints *)mediaStreamConstraintsPeerConnection {
    //
    RTCPair *maxWidth1 = [[RTCPair alloc] initWithKey:@"maxWidth" value:@"960"];
    RTCPair *maxHeight1 = [[RTCPair alloc] initWithKey:@"maxHeight" value:@"360"];
    //
    RTCPair *minWidth1 = [[RTCPair alloc] initWithKey:@"minWidth" value:@"960"];
    RTCPair *minHeight1 = [[RTCPair alloc] initWithKey:@"minHeight" value:@"360"];
    //
    
    NSArray *mandatoryConstraints = @[

                                      //                                      [[RTCPair alloc] initWithKey:@"DtlsSrtpKeyAgreement" value:@"false"],//false = ko dung ma hoa
                                      [[RTCPair alloc] initWithKey:@"DtlsSrtpKeyAgreement" value:@"false"],//false =>> Firefox bat buoc true
                                      maxWidth1,
                                      maxHeight1,
                                      minWidth1,
                                      minHeight1
                                      ];
    NSArray *optionalConstraints = @[
                                     [[RTCPair alloc] initWithKey:@"googCpuOveruseDetection" value:@"false"]//bat buoc o optional (PeerConnection)
                                     ];
    RTCMediaConstraints *constrains = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:optionalConstraints];
    return constrains;
}

//- (RTCMediaConstraints *)mediaStreamConstraintsPeerConnection {
//    NSArray *mandatoryConstraints = @[
//                                      //
//                                      [[RTCPair alloc] initWithKey:@"googCpuOveruseDetection" value:@"false"],
//                                      [[RTCPair alloc] initWithKey:@"googHighStartBitrate" value:@"10000"],
//                                      //
//                                      [[RTCPair alloc] initWithKey:@"DtlsSrtpKeyAgreement" value:@"false"],//false = ko dung ma hoa
//                                      ];
//    RTCMediaConstraints *constrains = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
//    return constrains;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    NSLog(@"didReceiveMemoryWarning");
}




- (IBAction)join:(id)sender {
    [[Manager instance].tcpClient sendDictionary:@{@"roomId": @(self.txtRoomId.text.intValue)} serviceType:ServiceType_JOIN_ROOM];
    [self.txtRoomId resignFirstResponder];
}


@end
