//
//  LogUtil.m
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/22/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import "LogUtil.h"

@implementation LogUtil

+ (void) debugLog:(NSString *)args {
    NSLog(@"%@", args);
}

@end
