//
//  Manager.h
//  TestUsageGoogleWebRTC
//
//  Created by Dau Ngoc Huy on 1/20/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//


#import <Foundation/Foundation.h>

#import "MBProgressHUD.h"

#import "TcpClient.h"
#import "ServiceType.h"
#import "NetworkHandler.h"
#import "RTCPeerConnection.h"
#import <sys/utsname.h>
#import "AppDelegate.h"

#import "LogUtil.h"

#import "RTCCenter.h"


// Manager data, connection, util function

@interface Manager : NSObject

+ (Manager*)instance;

// Data

@property (strong, nonatomic) AppDelegate * appDelegate;

// Connection socket

@property (nonatomic) dispatch_queue_t queueForSlaveConnection;
@property (strong, nonatomic) TcpClient *tcpClient;
- (void)connectToSignalingServer;

// Util function

+ (void) showToastWithString:(NSString *)text withView:(UIView *)view;
+ (float) scrWi;
+ (float) scrHi;

+ (NSString*) machineName;
+ (BOOL)isSpeedDevice:(NSString*)deviceName;

@end
