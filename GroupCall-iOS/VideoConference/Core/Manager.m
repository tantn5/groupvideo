//
//  Manager.m
//  TestUsageGoogleWebRTC
//
//  Created by Dau Ngoc Huy on 1/20/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import "Manager.h"

@implementation Manager

static Manager *sharedMyManager = nil;

- (id)init{
    self = [super init];
    if (self) {
        // Data
        // Network
        self.queueForSlaveConnection = dispatch_queue_create("queue_slave_connection", NULL);
    }
    return self;
}

+ (Manager*)instance {
    @synchronized(self) {
        if (sharedMyManager == nil)
            sharedMyManager = [[self alloc] init];
    }
    return sharedMyManager;
}

// Network connection

- (void)connectToSignalingServer {
    
    NetworkHandler* handler = [[NetworkHandler alloc] init];
    
    [Manager instance].tcpClient =
    //[[TcpClient alloc] initWithRemoteHost:@"123.31.43.8" port:5555];
    //[[TcpClient alloc] initWithRemoteHost:@"192.168.1.131" port:5555];
    [[TcpClient alloc] initWithRemoteHost:@"192.168.0.107" port:5555];
    [Manager instance].tcpClient.ssl = NO;
    [Manager instance].tcpClient.sslServerName = @"bomchat.com";
    
    [Manager instance].tcpClient.autoReconnect = YES;
    [Manager instance].tcpClient.handler = handler;
    
    [[Manager instance].tcpClient createSocketThread:self.queueForSlaveConnection];
}

// Util function

+(void) showToastWithString:(NSString *)text withView:(UIView *)view{
    [self showToastWithString:text withView:view autoHide:YES];
}

+(void) showToastWithString:(NSString *)text withView:(UIView *)view autoHide:(BOOL)autoHide{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.opaque = YES;
    hud.opacity = 0.75f;
    hud.mode = MBProgressHUDModeText;
    hud.labelText = @"";
    hud.detailsLabelFont = [UIFont fontWithName:@"HelveticaNeue" size:15.0];
    hud.detailsLabelText = text;
    hud.margin = 15.f;
    hud.removeFromSuperViewOnHide = YES;
    if (autoHide) {
        [hud hide:YES afterDelay:1.5f];
    }
}
+ (float) scrWi {
    return [UIScreen mainScreen].bounds.size.width;
}
+ (float) scrHi {
    return [UIScreen mainScreen].bounds.size.height;
}

+ (NSString*) machineName {
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}

+ (BOOL)isSpeedDevice:(NSString*)deviceName {
    BOOL speedDevice = NO;
    
    if ([deviceName hasPrefix:@"iPod"]) {
        NSString* number = [deviceName substringWithRange:NSMakeRange(@"iPod".length, 1)];
        if (number.intValue >= 7) {
            speedDevice = YES;
        }
    } else if ([deviceName hasPrefix:@"iPhone"]) {
        NSString* number = [deviceName substringWithRange:NSMakeRange(@"iPhone".length, 1)];
        if (number.intValue >= 7) {
            speedDevice = YES;
        }
    } else if ([deviceName hasPrefix:@"iPad"]) {
        NSString* number = [deviceName substringWithRange:NSMakeRange(@"iPad".length, 1)];
        if (number.intValue >= 3) {
            speedDevice = YES;
        }
    } else {
        speedDevice = YES;
    }
    
    return speedDevice;
}

@end
