//
//  PeerConnection.h
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/22/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

#import "RTCPeerConnectionDelegate.h"
#import "RTCSessionDescriptionDelegate.h"
#import "RTCEAGLVideoView.h"
#import "RTCVideoTrack.h"
#import "RTCPeerConnection.h"
#import "RTCPeerConnectionInterface.h"
#import "RTCMediaStream.h"

#import "PeerConnectionHandler.h"

@class RemoteSDPDelegate;

// Manager a peer connection send or receive

@interface PeerConnection : NSObject <RTCPeerConnectionDelegate, RTCSessionDescriptionDelegate>

@property (nonatomic, strong) NSString *peerId;

- (id) initWithPeerConnectionId: (NSString *) peerId;

// RTC Connection

@property (nonatomic, strong) RTCPeerConnection *peerConnection;

@property (nonatomic, assign) int isSendConnection;

@property (nonatomic, strong) RTCSessionDescription *localSDP;

@property (nonatomic, strong) RTCSessionDescription *remoteSDP;

@property (nonatomic, strong) RemoteSDPDelegate *remoteSDPDelegate;

@property (nonatomic, strong) NSMutableArray *listICECandidatesLocal;

@property (nonatomic, strong) NSMutableArray *listICECandidatesRemote;

@property (nonatomic, strong) PeerConnectionHandler *handler;

@property (nonatomic, strong) RTCMediaStream *stream;

- (void) createPeerSendStream;

- (void) createPeerReceiveStream;

- (void) gotRemoteSDP:(NSDictionary *)remoteSDP;

- (void) gotRemoteICECandidate:(RTCICECandidate *)ice;

- (void) closeConnection;

@end
