//
//  PeerConnection.m
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/22/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RTCVideoCapturer.h"
#import "RTCMediaStream.h"
#import "RTCMediaConstraints.h"
#import "RTCPair.h"
#import "RTCSessionDescription.h"
#import "RTCICECandidate.h"

#import "PeerConnection.h"
#import "Manager.h"

@implementation PeerConnection

- (id) initWithPeerConnectionId: (NSString *) peerId {
    self = [super init];
    if (self) {
        self.peerId = peerId;
        self.listICECandidatesLocal = [[NSMutableArray alloc] init];
        self.listICECandidatesRemote = [[NSMutableArray alloc] init];
        self.isSendConnection = 0;
    }
    return self;

}

- (void) createPeerSendStream {
    self.isSendConnection = 1;
    [self createPeerConnection:true];
    
}

- (void) createPeerReceiveStream {
    
    [self createPeerConnection:false];
    
}

- (void) gotRemoteSDP:(NSDictionary *)remoteSDP {
    self.remoteSDP = [[RTCSessionDescription alloc] initWithType:remoteSDP[@"type"] sdp:remoteSDP[@"offer"]];
    
    [self.peerConnection setRemoteDescriptionWithDelegate:self sessionDescription: self.remoteSDP];
}

- (void) gotRemoteICECandidate:(RTCICECandidate *)ice {
    [self.peerConnection addICECandidate:ice];
}

- (void) createPeerConnection: (bool) isSendConnection {
    
    // Init RTCCenter
    
    [RTCCenter instance];
    
    // Init config
    
    RTCConfiguration* config = [[RTCConfiguration alloc] init];
    config.iceServers = [[RTCCenter instance] getIceServers];
    
    // Create peer connection
    
    self.peerConnection = [[RTCCenter instance].pcFactory
                           peerConnectionWithConfiguration:config
                                               constraints:[[RTCCenter instance] mediaStreamConstraintsPeerConnection]
                                                  delegate:self];
    
    // Generate unique media stream id
    
    NSString* mediaStreamID = [NSString stringWithFormat:@"ARDAMS%@", @""];

    // Send connection
    if (isSendConnection) {
        
        // Create media stream
        
        RTCMediaStream *localStream = [[RTCCenter instance].pcFactory mediaStreamWithLabel:mediaStreamID];
        
        // Create video media --> add to media stream
        
        AVCaptureDevice *device;
        AVCaptureDevicePosition camera = AVCaptureDevicePositionBack;
        
        for (AVCaptureDevice *captureDevice in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] ) {
            if (captureDevice.position == camera) {
                device = captureDevice;
                break;
            }
        }
        
        if (device) {
            
            NSString* trackID = [NSString stringWithFormat:@"ARDAMSv0%@", @""];
            
            RTCVideoCapturer *capturer = [RTCVideoCapturer capturerWithDeviceName:device.localizedName];            RTCVideoSource *videoSource = [[RTCCenter instance].pcFactory videoSourceWithCapturer:capturer constraints:[[RTCCenter instance] mediaStreamConstraintsLocalVideo]];
            
            RTCVideoTrack *videoTrack = [[RTCCenter instance].pcFactory videoTrackWithID:trackID source:videoSource];
            
            
            [localStream addVideoTrack:videoTrack];
            
        }
        
        // Audio Track
        
        RTCAudioTrack *audioTrack = [[RTCCenter instance].pcFactory audioTrackWithID:[NSString stringWithFormat:@"ARDAMSa0%@", @""]];
        [localStream addAudioTrack:audioTrack];
        
        // Add stream to send
        
        self.stream = localStream;
        
        [self.peerConnection addStream:localStream];
        
        //
        //[self.handler onAddStream:self.stream];
        //
    }
    
    // Create offer/answer
    
    RTCMediaConstraints *constraints2 = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:
                                         @[
                                           [[RTCPair alloc] initWithKey:@"OfferToReceiveAudio" value: (isSendConnection ? @"false" : @"true") ],
                                           [[RTCPair alloc] initWithKey:@"OfferToReceiveVideo" value: (isSendConnection?@"false" : @"true") ]
                                           ]
                                                                              optionalConstraints: nil];
    
    [self.peerConnection createOfferWithDelegate:self constraints:constraints2];
}

// Session Description Delegate:

- (void)peerConnection:(RTCPeerConnection *)peerConnection didCreateSessionDescription:(RTCSessionDescription *)sdp error:(NSError *)error {
    
    if (error) {
    }

    NSString* sdpWithH264 = sdp.description;
    
    RTCSessionDescription* newSdp = [[RTCSessionDescription alloc] initWithType:sdp.type sdp:sdpWithH264];
    
    [peerConnection setLocalDescriptionWithDelegate:self sessionDescription:newSdp];

    if (newSdp.description) {
        
        // Has SDP Local --> Send to Server
        
        [self.handler onSdpReceiver:newSdp];
        
    } else {
    }
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection didSetSessionDescriptionWithError:(NSError *)error {
    
    if (peerConnection.signalingState == RTCSignalingHaveLocalOffer) {
    }
    if(!error && peerConnection.signalingState == RTCSignalingHaveRemoteOffer){
        RTCMediaConstraints *constraints2 = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:
                                             @[
                                               [[RTCPair alloc] initWithKey:@"OfferToReceiveAudio" value:@"true"],
                                               [[RTCPair alloc] initWithKey:@"OfferToReceiveVideo" value:@"true"]
                                               ]
                                                                                  optionalConstraints: nil];
        [peerConnection createAnswerWithDelegate:self constraints:constraints2];
        
    }

}

// Peer Connection Delegate

// Signaling state changed

- (void)peerConnection:(RTCPeerConnection *)peerConnection signalingStateChanged:(RTCSignalingState)stateChanged {
}

// Peer connection has stream: audio or video

- (void)peerConnection:(RTCPeerConnection *)peerConnection addedStream:(RTCMediaStream *)stream {
    
    // Only for receive connection
    self.stream = stream;
    [self.handler onAddStream:stream];
    
}

// Peer connection has close stream: audio or video

- (void)peerConnection:(RTCPeerConnection *)peerConnection removedStream:(RTCMediaStream *)stream {
    
    // Only for receive connection
    [self.handler onRemoveStream:stream];
}

- (void)peerConnectionOnRenegotiationNeeded:(RTCPeerConnection *)peerConnection {
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection iceConnectionChanged:(RTCICEConnectionState)newState {
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection iceGatheringChanged:(RTCICEGatheringState)newState {
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection gotICECandidate:(RTCICECandidate *)candidate {

    // Send candidate
    [self.listICECandidatesLocal addObject:candidate];
    [self.handler onCandidateReceiver:candidate];
    
}

- (void)peerConnection:(RTCPeerConnection*)peerConnection didOpenDataChannel:(RTCDataChannel*)dataChannel {
}


// First Media packet received

- (void)peerConnectionOnFirstMediaPacketReceived:(RTCPeerConnection *)peerConnection {
    
}

- (void) closeConnection {
    [self.peerConnection close];
    if (self.stream) {
        NSArray *listVideoTracks = self.stream.videoTracks;
        NSArray *listAudioTracks = self.stream.audioTracks;
        for (int i = 0; i<listAudioTracks.count; i++) {
            [self.stream removeAudioTrack:listAudioTracks[i]];
        }
        for (int i = 0; i<listVideoTracks.count; i++) {
            [self.stream removeVideoTrack:listVideoTracks[i]];
        }        
    }
}

@end
