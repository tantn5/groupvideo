//
//  PeerConnectionHandler.h
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RTCICECandidate.h"
#import "RTCSessionDescription.h"
#import "RTCMediaStream.h"

@protocol PeerConnectionHandlerDelegate <NSObject>

- (void) receivedStream: (RTCMediaStream *) stream;

- (void) removedStream: (RTCMediaStream *) stream;

@end

@class PeerConnection;

@interface PeerConnectionHandler : NSObject

@property (nonatomic, weak) id <PeerConnectionHandlerDelegate> delegate;

@property (nonatomic, weak) PeerConnection *peerConnection;

- (void) onCandidateReceiver: (RTCICECandidate *) iceCandidate;

- (void) onSdpReceiver: (RTCSessionDescription *) sdp;

- (void) onRemoteSdpReceiver: (NSDictionary *) sdp;

- (void) onRemoteCandidateReceiver: (RTCICECandidate *) iceCandidate;

- (void) onAddStream: (RTCMediaStream *) stream;

- (void) onRemoveStream: (RTCMediaStream *) stream;

@end
