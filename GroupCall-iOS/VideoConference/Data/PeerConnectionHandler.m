//
//  PeerConnectionHandler.m
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import "PeerConnectionHandler.h"
#import "PeerConnection.h"

@implementation PeerConnectionHandler

- (void) onCandidateReceiver: (RTCICECandidate *) iceCandidate {
    // send candidate

    NSDictionary *dic = @{@"sdpMid": iceCandidate.sdpMid, @"sdpMLineIndex": @(iceCandidate.sdpMLineIndex), @"offer": iceCandidate.sdp};
    NSDictionary *packetDic = @{@"name": self.peerConnection.peerId, @"candidate": dic};
    
    [[Manager instance].tcpClient sendDictionary:packetDic serviceType:ServiceType_ICE_CANDIDATE];
}

- (void) onSdpReceiver: (RTCSessionDescription *) sdp {
    // send sdp
    if (sdp) {
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        dic[@"offer"] = sdp.description;
        
        if ([sdp.type isEqualToString:@"offer"]) {
            dic[@"type"] = @"offer";
        }
        else if ([sdp.type isEqualToString:@"answer"]) {
            dic[@"type"] = @"answer";
        }
        
        NSDictionary *packetDic = @{@"name": self.peerConnection.peerId, @"sdpOffer": dic};
        
        [[Manager instance].tcpClient sendDictionary:packetDic serviceType:ServiceType_SDP_OFFER];
        
    }
    
}

- (void) onRemoteSdpReceiver: (NSDictionary *) sdp {
    // set remote sdp    
    [self.peerConnection gotRemoteSDP:sdp];
}

- (void) onRemoteCandidateReceiver: (RTCICECandidate *) iceCandidate {
    // add remote candidate
    [self.peerConnection gotRemoteICECandidate:iceCandidate];
}

- (void) onAddStream: (id) stream {
    if (self.delegate) {
        [self.delegate receivedStream:stream];
    }
}

- (void) onRemoveStream: (id) stream {
    if (self.delegate) {
        [self.delegate removedStream:stream];
    }
}

@end
