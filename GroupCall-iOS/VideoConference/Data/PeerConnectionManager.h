//
//  PeerConnectionManager.h
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PeerConnection.h"

@interface PeerConnectionManager : NSObject

@property (nonatomic, strong) NSMutableDictionary *dicConnections;

+ (PeerConnectionManager*) instance;

- (PeerConnection *) createPeerConnection: (NSString *) peerId peerConnectionHandlerDelegate: (id) delegate isSendConnection: (bool) isSendConnection;

- (PeerConnection *) createPeerSendStream: (NSString *) peerId peerConnectionHandlerDelegate: (id) delegate;

- (PeerConnection *) createPeerReceiveStream: (NSString *) peerId peerConnectionHandlerDelegate: (id) delegate;

- (PeerConnection *) getPeerConnection: (NSString *) peerId;

- (void) removeAPeerConnection: (PeerConnection *) peerConnection;

@end
