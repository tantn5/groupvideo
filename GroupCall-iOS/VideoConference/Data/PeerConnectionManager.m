//
//  PeerConnectionManager.m
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import "PeerConnectionManager.h"
#import "PeerConnection.h"
#import "PeerConnectionHandler.h"

@implementation PeerConnectionManager

static PeerConnectionManager *sharedMyManager = nil;

+ (PeerConnectionManager*) instance {
    @synchronized(self) {
        if (sharedMyManager == nil)
            sharedMyManager = [[self alloc] init];
    }
    return sharedMyManager;
}

- (id) init {
    self = [super init];
    if (self) {
        self.dicConnections = [[NSMutableDictionary alloc] init];
    }
    return self;
    
}

- (PeerConnection *) createPeerSendStream:(NSString *)peerId peerConnectionHandlerDelegate:(id <PeerConnectionHandlerDelegate>) delegate {
    
    return [self createPeerConnection:peerId peerConnectionHandlerDelegate:delegate isSendConnection:YES];
    
}

- (PeerConnection *) createPeerReceiveStream:(NSString *)peerId peerConnectionHandlerDelegate:(id)delegate {
    
    return [self createPeerConnection:peerId peerConnectionHandlerDelegate:delegate isSendConnection:NO];
    
}

- (PeerConnection *) createPeerConnection:(NSString *)peerId peerConnectionHandlerDelegate:(id <PeerConnectionHandlerDelegate>) delegate isSendConnection:(bool)isSendConnection {
    
    PeerConnection *connection = [[PeerConnection alloc] initWithPeerConnectionId:peerId];
    PeerConnectionHandler *handler = [[PeerConnectionHandler alloc] init];
    handler.delegate = delegate;
    connection.handler = handler;
    handler.peerConnection = connection;
    
    self.dicConnections[peerId] = connection;
    
    if (isSendConnection) {
        [connection createPeerSendStream];
    }
    else {
        [connection createPeerReceiveStream];
    }
    
    return connection;
}

- (PeerConnection *) getPeerConnection: (NSString *) peerId {
    if (self.dicConnections) {
        return self.dicConnections[peerId];
    }
    return nil;
}

- (void) removeAPeerConnection:(PeerConnection *)peerConnection {
    if (self.dicConnections) {
        [self.dicConnections removeObjectForKey: peerConnection.peerId];
    }
}

@end
