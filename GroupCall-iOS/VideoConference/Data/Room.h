//
//  RoomHandler.h
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PeerConnectionHandler.h"
#import "RTCMediaStream.h"

@protocol RoomDelegate <NSObject>

- (void) joinRoomResponse: (NSDictionary *) info;
- (void) leaveRoomResponse: (NSDictionary *) info;
- (void) newMemberJoinRoom: (NSDictionary *) info;
- (void) receivedStream: (RTCMediaStream*) newStream;
- (void) removedStream: (RTCMediaStream*) stream;

@end

@interface Room : NSObject <PeerConnectionHandlerDelegate>

@property (nonatomic, weak) id <RoomDelegate> delegate;

@property (nonatomic, strong) NSString *roomId;

@property (nonatomic, strong) NSMutableArray *listUsers;

@property (nonatomic, strong) NSMutableDictionary *peerConnections;

- (void) joinRoom;

- (void) leaveRoom;

- (void) createSendPeerConnection: (NSString *) peerId;

- (void) createReceivePeerConnection: (NSString *) peerId;

- (void) processJoinRoomResponse: (NSDictionary *) info;

- (void) processLeaveRoomResponse: (NSDictionary *) info;

- (void) processNewMemberJoinRoom: (NSDictionary *) info;

@end
