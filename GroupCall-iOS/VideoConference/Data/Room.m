//
//  RoomHandler.m
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import "Room.h"
#import "PeerConnection.h"
#import "PeerConnectionManager.h"
#import "RoomManager.h"

@implementation Room

- (id) init {
    self = [super init];
    if (self) {
        self.roomId = @"";
        self.listUsers = [[NSMutableArray alloc] init];
        self.peerConnections = [[NSMutableDictionary alloc] init];
    }
    return self;
    
}

- (void) joinRoom {
    // send join room
    NSDictionary *packetDic = @{@"roomName": self.roomId, @"name": [[RTCCenter instance] getMyPeerConnectionId]};
    [[Manager instance].tcpClient sendDictionary:packetDic serviceType:ServiceType_JOIN_ROOM];
}

- (void) leaveRoom {
    // send leave room
    NSDictionary *packetDic = @{@"roomName": self.roomId};
    [[Manager instance].tcpClient sendDictionary:packetDic serviceType:ServiceType_LEAVE_ROOM];
}

- (void) createSendPeerConnection: (NSString *) peerId {
    PeerConnection *connection = [[PeerConnectionManager instance] createPeerSendStream:peerId peerConnectionHandlerDelegate:self];
    self.peerConnections[peerId] = connection;
}

- (void) createReceivePeerConnection: (NSString *) peerId {
    PeerConnection *connection = [[PeerConnectionManager instance] createPeerReceiveStream:peerId peerConnectionHandlerDelegate:self];
    self.peerConnections[peerId] = connection;
}

- (void) processJoinRoomResponse: (NSDictionary *) info {
    
    dispatch_async(dispatch_get_main_queue(), ^{        
        
        NSArray *listUsers = info[@"list"];
        [self.listUsers removeAllObjects];
        [self.listUsers addObject:listUsers];
        
        // Start peer connection to send local stream
        if ([[[RTCCenter instance] getMyPeerConnectionId] isEqualToString:@"phu"]) {
            [self createSendPeerConnection:[[RTCCenter instance] getMyPeerConnectionId]];
        }
        
        // Start many peer connection to receive remote stream
        for (int i = 0; i<listUsers.count; i++) {
            if ([listUsers[i] isEqualToString:[[RTCCenter instance] getMyPeerConnectionId]]) {
            }
            else {
                if ([listUsers[i] isEqualToString:@"phu"]) {
                    [self createReceivePeerConnection:listUsers[i]];
                }
            }
        }
        // Call delegate
        if (self.delegate) {
            [self.delegate joinRoomResponse:info];
        }
        //
        if ([[[RTCCenter instance] getMyPeerConnectionId] isEqualToString:@"phu"]) {
            PeerConnection *meConnection = [[PeerConnectionManager instance] getPeerConnection:[[RTCCenter instance] getMyPeerConnectionId]];
            [self.delegate receivedStream:meConnection.stream];
        }
        //
    });
}

- (void) processLeaveRoomResponse: (NSDictionary *) info {
    //
    NSString *userLeave = info[@"name"];
    //
    if ([userLeave isEqualToString:[[RTCCenter instance] getMyPeerConnectionId]]) {
        [self closeRoom];
    }
    else {
        PeerConnection *peerConnection = self.peerConnections[userLeave];
        [self removePeerConnection:peerConnection];
        if (self.delegate) {
            [self.delegate removedStream:peerConnection.stream];
        }
    }
    // Call delegate
    if (self.delegate) {
        [self.delegate leaveRoomResponse:info];
    }
}

- (void) removePeerConnection: (PeerConnection *) peerConnection {
    [peerConnection closeConnection];
    [self.peerConnections removeObjectForKey:peerConnection.peerId];
    [[PeerConnectionManager instance] removeAPeerConnection:peerConnection];
    [self.listUsers removeObject:peerConnection.peerId];
}

- (void) closeRoom {
    
    // Close peer connection: send my stream
    PeerConnection *connection = self.peerConnections[[[RTCCenter instance] getMyPeerConnectionId]];
    [self removePeerConnection:connection];
    
    // Close peer connection: receive remote stream
    NSArray *list = self.peerConnections.allValues;
    for (int i = 0; i<self.peerConnections.allValues.count; i++) {
        if ([self.peerConnections.allKeys[i] isEqualToString:[[RTCCenter instance] getMyPeerConnectionId]]) {
        }
        else {
            PeerConnection *connection = list[i];
            [self removePeerConnection:connection];
        }
    }
    
    //
    [[RoomManager instance] removeRoom:self];
}

- (void) processNewMemberJoinRoom: (id) info {
    // Who
    NSString *newMember = info[@"name"];
    //
    if ([newMember isEqualToString:@"phu"]) {
        return;
    }
    [self.listUsers addObject:newMember];
    //
    [self createReceivePeerConnection:newMember];
    // Call delegate
    if (self.delegate) {
        [self.delegate newMemberJoinRoom:info];
    }
}

// PeerConnectionHandler Delegate

- (void) receivedStream: (RTCMediaStream *) stream {
    if (self.delegate) {
        [self.delegate receivedStream:stream];
    }
}

- (void) removedStream: (RTCMediaStream *) stream {
    for (int i=0; i<self.peerConnections.allValues.count; i++) {
        PeerConnection *connection = self.peerConnections.allValues[i];
        if (connection.stream == stream) {
            [self removePeerConnection:connection];
            break;
        }
    }    
    //
    if (self.delegate) {
        [self.delegate removedStream:stream];
    }
}

@end

