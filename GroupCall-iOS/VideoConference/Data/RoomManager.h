//
//  RoomManager.h
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Room.h"

@interface RoomManager : NSObject

+ (RoomManager*)instance;

@property (nonatomic, strong) NSMutableDictionary *dicRooms;

- (Room *) getRoom: (NSString *) roomId;

- (Room *) createRoom: (NSString *) roomId roomDelegate: (id<RoomDelegate>) delegate;

- (void) removeRoom: (Room *) room;

@end
