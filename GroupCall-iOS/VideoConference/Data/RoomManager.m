//
//  RoomManager.m
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import "RoomManager.h"

@implementation RoomManager

static RoomManager *sharedMyManager = nil;

- (id)init{
    self = [super init];
    if (self) {
        self.dicRooms = [[NSMutableDictionary alloc] init];
    }
    return self;
}

+ (RoomManager*) instance {
    @synchronized(self) {
        if (sharedMyManager == nil)
            sharedMyManager = [[self alloc] init];
    }
    return sharedMyManager;
}

- (Room *) getRoom: (NSString *) roomId {
    if (self.dicRooms) {
        return self.dicRooms[roomId];
    }
    return nil;
}

- (void) removeRoom: (Room *) room {
    if (self.dicRooms) {
        [self.dicRooms removeObjectForKey:room.roomId];
    }
}

- (Room *) createRoom: (NSString *) roomId roomDelegate: (id<RoomDelegate>) delegate {
    Room *room = [[Room alloc] init];
    room.roomId = roomId;
    room.delegate = delegate;
    
    self.dicRooms[roomId] = room;
    
    return room;
}

@end
