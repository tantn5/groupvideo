//
//  AppDelegate.h
//  VideoConference
//
//  Created by Nguyen Van Phu on 5/19/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

