//
//  main.m
//  VideoConference
//
//  Created by Nguyen Van Phu on 5/19/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
