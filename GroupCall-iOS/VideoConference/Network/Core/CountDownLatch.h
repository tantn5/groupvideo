//
//  CountDownLatch.h
//  CountDownLatch
//
//  Created by Dau Ngoc Huy on 10/31/13.
//  Copyright (c) 2013 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>

// A key: wait connection master server success + get slave server success

@interface CountDownLatch : NSObject {
    int count;
    NSCondition* cond;
}

-(id)initWithCount:(int)count1;
-(void)await;
-(void)countDown;

@end
