//
//  CountDownLatch.m
//  CountDownLatch
//
//  Created by Dau Ngoc Huy on 10/31/13.
//  Copyright (c) 2013 Dau Ngoc Huy. All rights reserved.
//

#import "CountDownLatch.h"

@implementation CountDownLatch

-(id)initWithCount:(int)count1 {
    if (self = [super init]) {
        count = count1;
        cond = [[NSCondition alloc] init];
    }
    return self;
}

-(void)await {
    [cond lock];
    
    if (count > 0) {
        [cond wait];
    }
    
    [cond unlock];
}

-(void)countDown {
    [cond lock];
    
    count--;
    if (count == 0) {
        [cond signal];
    }
    
    [cond unlock];
}

@end
