//
//  NetworkHandlerBase.h
//  TestAsynSocket
//
//  Created by Dau Ngoc Huy on 3/30/13.
//  Copyright (c) 2013 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Packet.h"

@class TcpClient;

@interface NetworkHandlerBase : NSObject

- (void) onMessageReceived:(TcpClient*)tcpClient message:(Packet*)packet;
- (void) onDisconnected:(TcpClient*)tcpClient;
- (void) onConnected:(TcpClient*)tcpClient;

@end
