//
//  NetworkHandlerBase.m
//  TestAsynSocket
//
//  Created by Dau Ngoc Huy on 3/30/13.
//  Copyright (c) 2013 Dau Ngoc Huy. All rights reserved.
//

#import "NetworkHandlerBase.h"

@implementation NetworkHandlerBase

- (void) onMessageReceived:(TcpClient*)tcpClient message:(Packet*)packet {
    
}

- (void) onDisconnected:(TcpClient*)tcpClient {
    
}

- (void) onConnected:(TcpClient*)tcpClient {
    
}

@end
