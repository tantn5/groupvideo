//
//  Packet.h
//  ChatClient
//
//  Created by Huy Dau on 4/2/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef Packet_H
#define Packet_H

#import <Foundation/Foundation.h>

@interface Packet : NSObject {
	/**
	 * 2 byte2
	 */
	NSString* magic;
	/**
	 * 2 bytes
	 */
	unsigned short length;
	/**
	 * 2 bytes
	 */
	unsigned short service;
	/**
	 * Packet DATA
	 */
	NSData* body;
}

@property (nonatomic, strong) NSString* magic;
@property (nonatomic, assign) unsigned short length;
@property (nonatomic, assign) unsigned short service;
@property (nonatomic, strong) NSData* body;

- (id) initWithService:(unsigned short)serviceValue;
- (void) setBodyString:(NSString*)bodyString;
- (NSString*) toString;
- (id)decodeBodyAsJson;
- (id) decodeBodyAsArrays;

@end

#endif
