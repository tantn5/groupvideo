//
//  Packet.m
//  ChatClient
//
//  Created by Huy Dau on 4/2/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Packet.h"
//#import "Manager.h"
#include <netinet/in.h>

@implementation Packet

@synthesize magic, length, service, body;

- (id) initWithService:(unsigned short)serviceValue {
	self = [super init];
	if (self != nil) {
		self.magic = @"GC";
		self.service = (unsigned short)serviceValue;
		self.length = 0;
        body = nil;
	}
	return self;
}

- (void) setBodyString:(NSString*)bodyString {
    body = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
	length = (unsigned short) [body length] ;
}

- (id) decodeBodyAsJson {
    return [NSJSONSerialization JSONObjectWithData:body options:0 error:nil];
}

- (id) decodeBodyAsArrays {
    return [NSJSONSerialization JSONObjectWithData:body options: NSJSONReadingMutableContainers error:nil];
}

- (NSString*) toString {
	NSString *bd = [[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding];
	NSString* rt = [NSString stringWithFormat:@"Service=%d - Data=%@", service, bd];
	return rt;
}

@end
