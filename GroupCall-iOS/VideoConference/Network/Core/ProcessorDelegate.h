//
//  ProcessorDelegate.h
//  BoruaChat
//
//  Created by Huy Dau on 4/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Packet.h"

@protocol ProcessorDelegate <NSObject>
	
@required
- (void) process:(Packet*)packet;

@end
