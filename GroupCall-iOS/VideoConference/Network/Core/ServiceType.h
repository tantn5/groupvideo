//
//  ServiceType.h
//  BoruaChat
//
//  Created by Huy Dau on 4/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

// ServiceType network packet

typedef enum {
    
    ServiceType_LOGIN = 0,
    
    ServiceType_JOIN_ROOM = 1,
    ServiceType_LEAVE_ROOM = 2,
    ServiceType_SDP_OFFER = 3,
    ServiceType_SDP_ANSWER = 4,
    ServiceType_ICE_CANDIDATE = 5,
    ServiceType_NEW_MEMBER = 6,
    
} ServiceType;



