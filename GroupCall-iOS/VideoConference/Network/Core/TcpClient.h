//
//  TcpClient.h
//  TestAsynSocket
//
//  Created by Dau Ngoc Huy on 3/28/13.
//  Copyright (c) 2013 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Packet.h"
#import "NetworkHandlerBase.h"
#import "GCDAsyncSocket.h"
#import "CountDownLatch.h"
#import "ServiceType.h"

// Socket Client: Send and receive message

@interface TcpClient : NSObject {
    CountDownLatch* countDown;
}

// Socket connection

@property(nonatomic, strong) GCDAsyncSocket* socket;

// Auto reconnect

@property(nonatomic, assign) BOOL autoReconnect;
@property(nonatomic, assign) NSTimeInterval reconnectAfter;

// Address server

@property(nonatomic, strong) NSString* remoteHost;
@property(nonatomic, assign) int remotePort;

// Queue all message want to send

@property(nonatomic, strong) NSMutableArray* packetQueue;

// Handler process when receive message

@property(nonatomic, strong) NetworkHandlerBase* handler;

// Temp packet: used for process when receive message

@property(nonatomic, strong) Packet* tmpPacket;

// Dispatch queue: used for run all process related socket connection in other thread vs main thread --> no block ui

@property(nonatomic, assign) dispatch_queue_t socket_queue;

// SSL info connection

@property(nonatomic, assign) BOOL ssl;

@property(nonatomic, strong) NSString* sslServerName;

// Lastime receive packet: used to recreate new socket connection, because close by timeout

@property(nonatomic, strong) NSDate* lastTimeReceivedPacket;

@property(nonatomic, strong) NSCondition* condReconnectSleep;

// Create socket connection with thread GCD

- (void)createSocketThread:(dispatch_queue_t)socket_queue;
- (void)createSocketThreadAndWaitForFinishConnect:(dispatch_queue_t)socket_queue;

// Initial with address server

- (TcpClient*)initWithRemoteHost:(NSString*)host port:(int)port;

// Pop message from queue

- (void)canSendPacket;

// Close socket connection

- (void)close:(BOOL)now;

// Wakeup socket thread

- (void)wakeUpSocketThread;

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag;

/*
 Gui goi tin neu:
    - Socket da khoi tao va connected
    - Queue dang co so goi tin == 0
    - Da loggedIn
 neu khong thi dua goi tin vao queue
 */

- (void)send:(Packet*)packet;

- (void)sendDictionary:(NSDictionary*)jsonDictionary serviceType:(ServiceType)type ;

- (void)sendPacketImmediate:(Packet*)packet;

@end
