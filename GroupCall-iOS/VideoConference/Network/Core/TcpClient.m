//
//  TcpClient.m
//  TestAsynSocket
//
//  Created by Dau Ngoc Huy on 3/28/13.
//  Copyright (c) 2013 Dau Ngoc Huy. All rights reserved.
//

#import "TcpClient.h"
#import "ServiceType.h"
#import "Util.h"
#include <netinet/in.h>
#import "NetworkHandlerBase.h"

#define HEADER_LENGTH 6
#define PACKET_HEADER_TAG 1
#define PACKET_BODY_TAG 2

@implementation TcpClient

// Initial

- (TcpClient*)initWithRemoteHost:(NSString*)host port:(int)port {
	if ((self = [super init])) {
        self.autoReconnect = YES;
        countDown = nil;
        self.reconnectAfter = 0.25;
        self.remoteHost = host;
        self.remotePort = port;
        self.packetQueue = [[NSMutableArray alloc] init];
        self.condReconnectSleep = [[NSCondition alloc] init];
        self.ssl = YES;
	}
    
	return self;
}

// Create connection not wait

- (void)createSocketThread:(dispatch_queue_t)socket_queue {
    self.socket_queue = socket_queue;
    self.socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:socket_queue];
    [self.socket connectToHost:self.remoteHost onPort:self.remotePort error:nil];
    [((NetworkHandler *)self.handler) onConnectToServer];
    
    NSLog(@"Connecting to: %@:%d", self.remoteHost, self.remotePort);
}

// Create connection and wait done

- (void)createSocketThreadAndWaitForFinishConnect:(dispatch_queue_t)socket_queue {
    self.socket_queue = socket_queue;
    countDown = [[CountDownLatch alloc] initWithCount:1];
    self.socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:socket_queue];
    [self.socket connectToHost:self.remoteHost onPort:self.remotePort error:nil];
    [countDown await];
}

// Connection successfull

- (void)socket:(GCDAsyncSocket *)sender didConnectToHost:(NSString *)host port:(UInt16)port {
    
    // Use SSL
    
    if(self.ssl){
        
        // Connected to secure server (HTTPS)
        // Configure SSL/TLS settings
        
        NSMutableDictionary *settings = [NSMutableDictionary dictionaryWithCapacity:3];
        
        if (self.sslServerName) {
            [settings setObject:self.sslServerName forKey:(NSString *)kCFStreamSSLPeerName];
        }else{
            [settings setObject:@"example.com" forKey:(NSString *)kCFStreamSSLPeerName];
        }

        [sender startTLS:settings];
        
    }
    
    else{
        
        [self didConnectCompleted];
    }
}

- (void)didConnectCompleted {
    [self.handler onConnected:self];
    
    [self canSendPacket];
    
    [self.socket readDataToLength:HEADER_LENGTH withTimeout:-1 tag:PACKET_HEADER_TAG];
    
    self.reconnectAfter = 1.0;
    
    if (countDown) {
        [countDown countDown];
    }
}

- (void)socketDidSecure:(GCDAsyncSocket *)sock {
    [self didConnectCompleted];
}

// Connection disconnect

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err {
    
    if (countDown) {
        [countDown countDown];
    }

    [self.handler onDisconnected:self];
    
    if (self.autoReconnect) {
        
        [self.condReconnectSleep lock];
        [self.condReconnectSleep waitUntilDate:[NSDate dateWithTimeIntervalSinceNow:self.reconnectAfter]];
        [self.condReconnectSleep unlock];
        
        if (!self.socket.isConnected) {
            self.reconnectAfter = self.reconnectAfter*1.5;
            if (self.reconnectAfter > 55) {
                self.reconnectAfter = 55;
            }
            [self createSocketThread:self.socket_queue];
        }
    }
}

- (void)wakeUpSocketThread {
    [self.condReconnectSleep lock];
    [self.condReconnectSleep signal];
    [self.condReconnectSleep unlock];
}

- (void)send:(Packet*)packet {
    [self.packetQueue addObject:packet];
    if (self.socket_queue) {
        dispatch_async(self.socket_queue, ^{
            [self canSendPacket];
        });
    }
}

- (void)sendDictionary:(NSDictionary*)jsonDictionary serviceType:(ServiceType)type {
    NSData *data = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:0 error:nil];
    
    Packet* packet = [[Packet alloc] initWithService:type];
	packet.body = data;
    packet.length = (unsigned short) data.length;
    [self send:packet];
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag {
    [self canSendPacket];
}

- (void)canSendPacket {
    if (self.socket && [self.socket isConnected]) {
        if (self.packetQueue.count > 0) {
            [self sendPacketImmediate:self.packetQueue[0]];
            [self.packetQueue removeObjectAtIndex:0];
        }
    }
}

- (void)close:(BOOL)now {
    if (now) {
        [self.socket disconnect];
    } else {
        [self.socket disconnectAfterWriting];
    }
}

- (void)sendPacketImmediate:(Packet*)packet {
    NSMutableData* outgoingDataBuffer = [[NSMutableData alloc] init];
    
    //magic
    NSData* magicData = [packet.magic dataUsingEncoding:NSUTF8StringEncoding];
    [outgoingDataBuffer appendData:magicData];
    
    //length
    unsigned short net_length = htons(packet.length);
    [outgoingDataBuffer appendBytes:(const uint8_t*)&net_length length:2];
    
    //service
    unsigned short net_service = htons(packet.service);
    [outgoingDataBuffer appendBytes:(const uint8_t*)&net_service length:2];
    
    //body
    if (packet.length > 0) {
        [outgoingDataBuffer appendData:packet.body];
    }
	
	//Try to write to stream
	[self.socket writeData:outgoingDataBuffer withTimeout:-1 tag:0];
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag {
    if (tag == PACKET_HEADER_TAG) {//decode header
        //Doc Length
        unsigned short length;
        [data getBytes:(uint8_t*)&length range:NSMakeRange(2, 2)];
        length = htons(length);
        
        //Doc service
        unsigned short service;
        [data getBytes:(uint8_t*)&service range:NSMakeRange(4, 2)];
        service = htons(service);
        
        self.tmpPacket = [[Packet alloc] initWithService:service];
        self.tmpPacket.length = length;
        
        if (length > 0) {
            // Read body
            [self.socket readDataToLength:length withTimeout:-1 tag:PACKET_BODY_TAG];
        } else {
            //Cho` doc header goi tin ke tiep
            [self.socket readDataToLength:HEADER_LENGTH withTimeout:-1 tag:PACKET_HEADER_TAG];
            [self.handler onMessageReceived:self message:self.tmpPacket];
        }
    } else if(tag == PACKET_BODY_TAG){//decode body
        self.tmpPacket.body = data;
        
        [self.handler onMessageReceived:self message:self.tmpPacket];
        
        //Cho` du lieu den: cho` header goi tin ke tiep
        [self.socket readDataToLength:HEADER_LENGTH withTimeout:-1 tag:PACKET_HEADER_TAG];
    }
}

@end
