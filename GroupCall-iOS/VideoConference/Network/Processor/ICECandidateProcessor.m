//
//  ICECandidateProcessor.m
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import "ICECandidateProcessor.h"
#import "PeerConnection.h"
#import "PeerConnectionManager.h"
#import "RTCICECandidate.h"

@implementation ICECandidateProcessor

- (void) process:(Packet*)packet {
    NSDictionary* res = [packet decodeBodyAsJson];
    
    NSString *name = res[@"name"];
    NSDictionary *dicCandidate = res[@"candidate"];
    
    if (name && name.length > 0) {
        NSString *sdpMid = dicCandidate[@"sdpMid"];
        NSNumber *sdpMLineIndex = dicCandidate[@"sdpMLineIndex"];
        NSString *candidate = dicCandidate[@"candidate"];
        
        RTCICECandidate *iceCandidate = [[RTCICECandidate alloc] initWithMid:sdpMid
                                                                       index:sdpMLineIndex.intValue
                                                                         sdp:candidate];
        
        PeerConnection *peerConnection = [[PeerConnectionManager instance] getPeerConnection:name];
        
        [peerConnection.handler onRemoteCandidateReceiver:iceCandidate];
    }
}

@end
