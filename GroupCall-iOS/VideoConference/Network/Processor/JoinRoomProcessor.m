//
//  JoinRoomProcessor.m
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import "JoinRoomProcessor.h"
#import "Room.h"
#import "RoomManager.h"

@implementation JoinRoomProcessor

- (void) process:(Packet*)packet {
    NSDictionary* res = [packet decodeBodyAsJson];
    
    NSString *roomName = res[@"roomName"];
    
    if (roomName && roomName.length > 0) {
        
        Room *room = [[RoomManager instance] getRoom:roomName];
    
        [room processJoinRoomResponse:res];
        
    }
    
}

@end
