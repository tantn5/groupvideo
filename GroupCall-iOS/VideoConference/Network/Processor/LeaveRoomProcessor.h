//
//  LeaveRoomProcessor.h
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProcessorDelegate.h"

@interface LeaveRoomProcessor : NSObject <ProcessorDelegate>

- (void) process:(Packet*)packet;

@end
