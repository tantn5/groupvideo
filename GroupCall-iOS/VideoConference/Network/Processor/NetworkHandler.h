//
//  NetworkHandler.h
//  CaroGame
//
//  Created by Dau Ngoc Huy on 8/21/14.
//  Copyright (c) 2014 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Packet.h"
#import "TcpClient.h"
#import "NetworkHandlerBase.h"

@protocol NetworkHandlerDelegate <NSObject>

- (void) onConnected;
- (void) onDisconnected;
- (void) onConnectToServer;

@end

typedef enum {
    
    SocketConnectionStatus_Connecting = 0,
    
    SocketConnectionStatus_Connected = 1,
    
    SocketConnectionStatus_DisConnected = 2,
    
} SocketConnectionStatus;


@interface NetworkHandler : NetworkHandlerBase {
    NSMutableDictionary* processors;
    SocketConnectionStatus connectStatus;
}

@property (nonatomic, weak) id <NetworkHandlerDelegate> delegate;

- (void) onMessageReceived:(TcpClient*)tcpClient message:(Packet*)packet;
- (void) onDisconnected:(TcpClient*)tcpClient;
- (void) onConnected:(TcpClient*)tcpClient;

- (void) onConnectToServer;

- (SocketConnectionStatus) getNetworkConnectionStatus;

@end
