//
//  NetworkHandler.m
//  CaroGame
//
//  Created by Dau Ngoc Huy on 8/21/14.
//  Copyright (c) 2014 Dau Ngoc Huy. All rights reserved.
//

#import "NetworkHandler.h"
#import "Util.h"
#import "ProcessorDelegate.h"
#import "Manager.h"

#import "SDPAnswerProcessor.h"
#import "JoinRoomProcessor.h"
#import "LeaveRoomProcessor.h"
#import "SDPAnswerProcessor.h"
#import "ICECandidateProcessor.h"
#import "NewMemberProcessor.h"

@implementation NetworkHandler

- (id) init {
	self = [super init];
	if (self != nil) {
        
        connectStatus = SocketConnectionStatus_Connecting;
        
		processors = [[NSMutableDictionary alloc] init];
        
        processors[@(ServiceType_JOIN_ROOM)] = [[JoinRoomProcessor alloc] init];
        processors[@(ServiceType_LEAVE_ROOM)] = [[LeaveRoomProcessor alloc] init];
        processors[@(ServiceType_SDP_ANSWER)] = [[SDPAnswerProcessor alloc] init];
        processors[@(ServiceType_ICE_CANDIDATE)] = [[ICECandidateProcessor alloc] init];
        processors[@(ServiceType_NEW_MEMBER)] = [[NewMemberProcessor alloc] init];
	}
	return self;
}

- (void) onMessageReceived:(TcpClient*)tcpClient message:(Packet*)packet {
    unsigned short service = packet.service;
    id processor = processors[[NSNumber numberWithInt:service]];
    if (processor) {
        [processor process:packet];
    } else {
        NSLog(@"Error, processor not found for service: %d", packet.service);
    }
}

- (void) onDisconnected:(TcpClient*)tcpClient {
    connectStatus = SocketConnectionStatus_DisConnected;
    if (self.delegate) {
        [self.delegate onDisconnected];
    }
}

- (void) onConnected:(TcpClient*)tcpClient {
    connectStatus = SocketConnectionStatus_Connected;
    if (self.delegate) {
        [self.delegate onConnected];
    }
}

- (SocketConnectionStatus) getNetworkConnectionStatus {
    return connectStatus;
}

- (void) onConnectToServer {
    connectStatus = SocketConnectionStatus_Connecting;
    if (self.delegate) {
        [self.delegate onConnectToServer];
    }

}

@end
