//
//  NewMemberProcessor.m
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import "NewMemberProcessor.h"
#import "Room.h"
#import "RoomManager.h"

@implementation NewMemberProcessor

- (void) process:(Packet*)packet {
    NSDictionary* res = [packet decodeBodyAsJson];
    NSString *roomId = res[@"roomName"];
    if (roomId && roomId.length > 0) {
        Room *room = [[RoomManager instance] getRoom:roomId];
        [room processNewMemberJoinRoom:res];
    }
}

@end
