//
//  SDPAnswerProcessor.m
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/23/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import "SDPAnswerProcessor.h"
#import "PeerConnection.h"
#import "PeerConnectionManager.h"

@implementation SDPAnswerProcessor

- (void) process:(Packet*)packet {
    
    NSDictionary* res = [packet decodeBodyAsJson];
    
    NSString *name = res[@"name"];
    NSDictionary *sdpAnswer = res[@"sdpAnswer"];
    
    if (name && name.length > 0) {
        
        PeerConnection *peerConnection = [[PeerConnectionManager instance] getPeerConnection:name];
    
        [peerConnection.handler onRemoteSdpReceiver:sdpAnswer];
        
    }
}

@end
