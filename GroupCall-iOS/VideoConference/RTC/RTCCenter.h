//
//  RTCCenter.h
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/22/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RTCPeerConnectionFactory.h"

@interface RTCCenter : NSObject

+ (RTCCenter*)instance;

- (NSArray*)getIceServers;

@property (nonatomic, strong) RTCPeerConnectionFactory *pcFactory;

+ (NSString *)preferVP8:(NSString *)sdpString;

+ (NSString *)preferVP9:(NSString *)sdpString;

+ (NSString *)preferH264:(NSString *)sdpString;

- (RTCMediaConstraints *)mediaStreamConstraintsPeerConnection;

- (RTCMediaConstraints *)mediaStreamConstraintsLocalVideo;

- (NSString *) getMyPeerConnectionId;

- (NSString *) getRoomId;

@end
