//
//  RTCCenter.m
//  VideoConference
//
//  Created by Nguyen Van Phu on 12/22/16.
//  Copyright © 2016 VideoConference. All rights reserved.
//

#import "RTCCenter.h"
#import "RTCICEServer.h"
#import "RTCPair.h"
#import "RTCMediaConstraints.h"

// RTCCenter: All resource and data about rtc in here

@implementation RTCCenter

static RTCCenter *sharedMyManager = nil;

// Initial + RTCPeerConnectionFactory initial

- (id)init{
    self = [super init];
    if (self) {
        [RTCPeerConnectionFactory initializeSSL];
        if (!self.pcFactory) {
            self.pcFactory = [[RTCPeerConnectionFactory alloc] init];
        }

    }
    return self;
}

+ (RTCCenter*)instance {
    @synchronized(self) {
        if (sharedMyManager == nil)
            sharedMyManager = [[self alloc] init];
    }
    return sharedMyManager;
}

// ICEServers

- (NSArray*)getIceServers {
    NSMutableArray *servers = [[NSMutableArray alloc] init];
    
//    RTCICEServer *server1 = [[RTCICEServer alloc] initWithURI:[NSURL URLWithString:@"stun:nat2.azstack.com:3478"] username:@"" password:@""];
//    [servers addObject:server1];
    
    return servers;
}

// Media stream constraints in peer connection

- (RTCMediaConstraints *)mediaStreamConstraintsPeerConnection {
    //
    RTCPair *maxWidth1 = [[RTCPair alloc] initWithKey:@"maxWidth" value:@"960"];
    RTCPair *maxHeight1 = [[RTCPair alloc] initWithKey:@"maxHeight" value:@"720"];
    //
    RTCPair *minWidth1 = [[RTCPair alloc] initWithKey:@"minWidth" value:@"480"];
    RTCPair *minHeight1 = [[RTCPair alloc] initWithKey:@"minHeight" value:@"320"];
    //
    
    NSArray *mandatoryConstraints = @[
                                      [[RTCPair alloc] initWithKey:@"DtlsSrtpKeyAgreement" value:@"true"],
                                      maxWidth1,
                                      maxHeight1,
                                      minWidth1,
                                      minHeight1
                                      ];
    NSArray *optionalConstraints = @[
                                     [[RTCPair alloc] initWithKey:@"googCpuOveruseDetection" value:@"false"]
                                     ];
    RTCMediaConstraints *constrains = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:optionalConstraints];
    return constrains;
}

// Media stream constraint in local

- (RTCMediaConstraints *)mediaStreamConstraintsLocalVideo {

    BOOL speedDevice = [Manager isSpeedDevice:[Manager machineName]];
    
    NSString *minWidth;
    NSString *minHeight;
    
    NSString *maxWidth;
    NSString *maxHeight;
    
    //
    if(speedDevice){
        minWidth = @"640";
        minHeight = @"480";
    }else{
        minWidth = @"640";
        minHeight = @"480";
    }
    //
    if(speedDevice){
        maxWidth = @"960";
        maxHeight = @"720";
    }else{
        maxWidth = @"960";
        maxHeight = @"720";
    }
    
    //
    RTCPair *maxWidth1 = [[RTCPair alloc] initWithKey:@"maxWidth" value:maxWidth];
    RTCPair *maxHeight1 = [[RTCPair alloc] initWithKey:@"maxHeight" value:maxHeight];
    //
    RTCPair *minWidth1 = [[RTCPair alloc] initWithKey:@"minWidth" value:minWidth];
    RTCPair *minHeight1 = [[RTCPair alloc] initWithKey:@"minHeight" value:minHeight];
    //
    NSArray *videoConstraints = @[maxWidth1, maxHeight1, minWidth1, minHeight1];
    return [[RTCMediaConstraints alloc] initWithMandatoryConstraints:videoConstraints optionalConstraints:nil];
}


// Prefer Codec Video

+ (NSString *)preferVideoCodec:(NSString *)codec inSDP:(NSString *)sdpString
{
    NSString *lineSeparator = @"\n";
    NSString *mLineSeparator = @" ";
    NSMutableArray *lines = [NSMutableArray arrayWithArray:[sdpString componentsSeparatedByString:lineSeparator]];
    NSInteger mLineIndex = -1;
    NSString *codecRtpMap = nil;
    NSString *pattern = [NSString stringWithFormat:@"^a=rtpmap:(\\d+) %@(/\\d+)+[\r]?$", codec];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                           options:0
                                                                             error:nil];
    for (NSInteger i = 0; (i < lines.count) && (mLineIndex == -1 || !codecRtpMap); ++i) {
        NSString *line = lines[i];
        if ([line hasPrefix:@"m=video"]) {
            mLineIndex = i;
            continue;
        }
        NSTextCheckingResult *codecMatches = [regex firstMatchInString:line
                                                               options:0
                                                                 range:NSMakeRange(0, line.length)];
        if (codecMatches) {
            codecRtpMap = [line substringWithRange:[codecMatches rangeAtIndex:1]];
            continue;
        }
    }
    
    if (mLineIndex == -1) {
        NSLog(@"No m=video line, so can't prefer %@", codec);
        return sdpString;
    }
    
    if (!codecRtpMap) {
        NSLog(@"No rtpmap for %@", codec);
        return sdpString;
    }
    
    NSArray *origMLineParts = [lines[mLineIndex] componentsSeparatedByString:mLineSeparator];
    
    if (origMLineParts.count > 3) {
        NSMutableArray *newMLineParts = [NSMutableArray arrayWithCapacity:origMLineParts.count];
        NSInteger origPartIndex = 0;
        [newMLineParts addObject:origMLineParts[origPartIndex++]];
        [newMLineParts addObject:origMLineParts[origPartIndex++]];
        [newMLineParts addObject:origMLineParts[origPartIndex++]];
        [newMLineParts addObject:codecRtpMap];
        
        for (; origPartIndex < origMLineParts.count; ++origPartIndex) {
            if (![codecRtpMap isEqualToString:origMLineParts[origPartIndex]]) {
                [newMLineParts addObject:origMLineParts[origPartIndex]];
            }
        }
        NSString *newMLine = [newMLineParts componentsJoinedByString:mLineSeparator];
        [lines replaceObjectAtIndex:mLineIndex withObject:newMLine];
    }
    else {
        NSLog(@"Wrong SDP media description format: %@", lines[mLineIndex]);
    }
    
    return [lines componentsJoinedByString:lineSeparator];
}

+ (NSString *)preferVP8:(NSString *)sdpString
{
    return [self preferVideoCodec:@"VP8" inSDP:sdpString];
}

+ (NSString *)preferVP9:(NSString *)sdpString {
    return [self preferVideoCodec:@"VP9" inSDP:sdpString];
}

+ (NSString *)preferH264:(NSString *)sdpString
{
    return [self preferVideoCodec:@"H264" inSDP:sdpString];
}

- (NSString *) getMyPeerConnectionId {
    
    return @"phu_20";
}

- (NSString *) getRoomId {
    return @"RoomTest";
}

@end
